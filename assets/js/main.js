jQuery(document).ready(function($) {

    jQuery("#questions_type").change(function(){
        var questions_type = jQuery(this[this.selectedIndex]).val();
       if(questions_type == 'mcqs'){
           jQuery('.mcq_table').show();
           jQuery('.form-control_table').prop('required',true);

       }else{
           jQuery('.mcq_table').hide();
           jQuery('.form-control_table').prop('required',false);
       }
    });

    jQuery('.mcqsoption').on('keyup keypress', function(e) {
        var mcqoption = jQuery(this).attr("name");
        var mcqoptionvalue = jQuery(this).val();
        jQuery('#'+mcqoption).val(mcqoptionvalue);


    });

});
