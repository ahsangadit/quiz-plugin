<?php

abstract class Vgc_Question_Meta_Box {


    /**
     * Set up and add the meta box.
     */
    public static function add() {
        $screens = [ 'post', 'questions' ];
        foreach ( $screens as $screen ) {
            add_meta_box(
                'Vgcquestions_box_id',          // Unique ID
                'Paremeters', // Box title
                [ self::class, 'html' ],   // Content callback, must be of type callable
                $screen                  // Post type
            );
        }
    }


    /**
     * Save the meta box selections.
     *
     * @param int $post_id  The post ID.
     */
    public static function save( int $post_id ) {

        if ( array_key_exists( 'questions_programid', $_POST ) ) {
            update_post_meta(
                $post_id,
                'questions_programid',
                $_POST['questions_programid']
            );
        }
        if ( array_key_exists( 'questions_type', $_POST ) ) {
            update_post_meta(
                $post_id,
                'questions_type',
                $_POST['questions_type']
            );
        }
        if ( array_key_exists( 'questions_type', $_POST ) ) {
            update_post_meta(
                $post_id,
                'questions_type',
                $_POST['questions_type']
            );
        }
        if ( array_key_exists( 'option1', $_POST ) ) {
            update_post_meta(
                $post_id,
                'option1',
                $_POST['option1']
            );
        }
        if ( array_key_exists( 'option2', $_POST ) ) {
            update_post_meta(
                $post_id,
                'option2',
                $_POST['option2']
            );
        }
        if ( array_key_exists( 'option3', $_POST ) ) {
            update_post_meta(
                $post_id,
                'option3',
                $_POST['option3']
            );
        }
        if ( array_key_exists( 'option4', $_POST ) ) {
            update_post_meta(
                $post_id,
                'option4',
                $_POST['option4']
            );
        }
        if ( array_key_exists( 'correct_ans', $_POST ) ) {
            update_post_meta(
                $post_id,
                'correct_ans',
                $_POST['correct_ans']
            );
        }
        if ( array_key_exists( 'question_timelimit', $_POST ) ) {
            update_post_meta(
                $post_id,
                'question_timelimit',
                $_POST['question_timelimit']
            );

        }
        if ( array_key_exists( 'question_category', $_POST ) ) {
            update_post_meta(
                $post_id,
                'question_category',
                $_POST['question_category']
            );

        }



    }


    /**
     * Display the meta box HTML to the user.
     *
     * @param \WP_Post $post   Post object.
     */
    public static function html( $post ) {


        $posts_programs = get_posts([
            'post_type' => 'Programs',
            'post_status' => 'publish',
            'numberposts' => -1
            // 'order'    => 'ASC'
        ]);
        $posts_tests = get_posts([
            'post_type' => 'tests',
            'post_status' => 'publish',
            'numberposts' => -1
            // 'order'    => 'ASC'
        ]);
        $posts_category = get_posts([
            'post_type' => 'question_category',
            'post_status' => 'publish',
            'numberposts' => -1
            // 'order'    => 'ASC'
        ]);
//        echo '<pre>';
//        var_dump($posts_programs);
//        echo '</pre>';

        $questions_programid = get_post_meta( $post->ID, 'questions_programid', true );
        $question_timelimit = get_post_meta( $post->ID, 'question_timelimit', true );
        $questions_type = get_post_meta( $post->ID, 'questions_type', true );
        $question_category = get_post_meta( $post->ID, 'question_category', true );
        $option1 = get_post_meta( $post->ID, 'option1', true );
        $option2 = get_post_meta( $post->ID, 'option2', true );
        $option3 = get_post_meta( $post->ID, 'option3', true );
        $option4 = get_post_meta( $post->ID, 'option4', true );
        $correct_ans = get_post_meta( $post->ID, 'correct_ans', true );



        ?>

        <div class="row">


            <div class="col-md-12" >
                <label for="questions_type">Select Program Type</label>
                <select name="questions_type" id="questions_type" class="form-control" required>

                    <option value="mcqs" <?php selected( $questions_type, 'mcqs'); ?>>Mcqs</option>
                    <option value="visualise" <?php selected( $questions_type, 'visualise'); ?>>Visualise</option>
                    <option value="grammer" <?php selected( $questions_type, 'grammer'); ?>>Grammer</option>


                </select>
            </div>
            <div class="col-md-12" >
                <label for="questions_type">Select Category Type</label>
                <select name="question_category" id="question_category" class="form-control">
                            <option value="">Select Category</option>
                            <?php foreach($posts_category as $p){ ?>
                                <option value="<?php echo $p->ID ?>"  <?php selected( $question_category, $p->ID ); ?>><?php echo $p->post_title ?></option>
                                <?php } ?>
                 </select>
            </div>
            <div class="col-md-12">
                <label for="question_timelimit">Time Limit (in mins)</label>
                <input type="number" id="question_timelimit"  class="form-control" name="question_timelimit"  value="<?php if(isset($question_timelimit)){ echo $question_timelimit;  }  ?>" min="1"  required>
            </div>

            <div class="col-md-12" style="margin-top: 15px;">
                <table class="wp-list-table widefat plugins table mcq_table" <?php  if($questions_type == 'visualise'){  echo 'style="display:none!important";';  }  ?>>
                    <thead>
                    <tr class="mcq_table_th_row" >
                        <th>Option #</th>
                        <th >Option Name</th>
                        <th>Correct Answer</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr class="mcq_table_row">
                        <td>  1 </td>
                        <td><input type="text" name="option1" class="form-control_table mcqsoption" value="<?php if(isset($option1)){ echo $option1;  }  ?>" <?php if($questions_type == 'mcqs'){ ?>required <?php } ?>></td>
                        <td ><input type="radio" id="option1" name="correct_ans" <?php if($correct_ans == ''){ echo'value="option1"'; } else {  echo'value="'.$option1.'"'; } ?>  <?php if($correct_ans == ''){ echo 'checked'; } else{ checked( $option1,$correct_ans );  } ?> ></td>
                    </tr>
                    <tr class="mcq_table_row">
                        <td> 2 </td>
                        <td><input type="text" name="option2" class="form-control_table mcqsoption" value="<?php if(isset($option2)){ echo $option2;  }  ?>" <?php if($questions_type == 'mcqs'){ ?>required <?php } ?>></td>
                        <td ><input type="radio" id="option2" name="correct_ans"  <?php if($correct_ans == ''){ echo'value="option2"'; } else {  echo'value="'.$option2.'"'; } ?>  <?php if($correct_ans != ''){ checked( $option2,$correct_ans );} ?>  ></td>
                    </tr>
                    <tr class="mcq_table_row">
                        <td>  3 </td>
                        <td><input type="text" name="option3" class="form-control_table mcqsoption" value="<?php if(isset($option3)){ echo $option3;  }  ?>" <?php if($questions_type == 'mcqs'){ ?>required <?php } ?>></td>
                        <td ><input type="radio" id="option3" name="correct_ans"  <?php if($correct_ans == ''){ echo'value="option3"'; } else {  echo'value="'.$option3.'"'; } ?>  <?php if($correct_ans != ''){ checked($option3,$correct_ans );} ?>></td>
                    </tr>
                    <tr class="mcq_table_row">
                        <td>  4 </td>
                        <td><input type="text" name="option4" class="form-control_table mcqsoption" value="<?php if(isset($option4)){ echo $option4;  }  ?>" <?php if($questions_type == 'mcqs'){ ?>required <?php } ?>></td>
                        <td ><input type="radio" id="option4" name="correct_ans"  <?php if($correct_ans == ''){ echo'value="option4"'; } else {  echo'value="'.$option4.'"'; } ?>  <?php if($correct_ans != ''){ checked( $option4,$correct_ans );} ?>></td>
                    </tr>


                    </tbody>

                </table>
            </div>

        </div>
        <?php
    }
}

add_action( 'add_meta_boxes', [ 'Vgc_Question_Meta_Box', 'add' ] );
add_action( 'save_post', [ 'Vgc_Question_Meta_Box', 'save' ] );
