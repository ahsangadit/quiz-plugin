<?php

abstract class Vgc_Test_Meta_Box {



    /**
     * Set up and add the meta box.
     */
    public static function add() {
        $screens = [ 'post', 'tests' ];
        foreach ( $screens as $screen ) {
            add_meta_box(
                'Vgctest_box_id',          // Unique ID
                'Paremeters', // Box title
                [ self::class, 'html' ],   // Content callback, must be of type callable
                $screen                  // Post type
            );
        }
    }


    /**
     * Save the meta box selections.
     *
     * @param int $post_id  The post ID.
     */
    public static function save( int $post_id ) {
        // var_dump(array_key_exists( 'mcqs', $_POST ) );
        // die;
        if ( array_key_exists( 'test_timelimit', $_POST ) ) {
            update_post_meta(
                $post_id,
                'test_timelimit',
                $_POST['test_timelimit']
            );

        }
        if ( array_key_exists( 'test_passinglevel', $_POST ) ) {
            update_post_meta(
                $post_id,
                'test_passinglevel',
                $_POST['test_passinglevel']
            );

        }
        if ( array_key_exists( 'test_total_marks', $_POST ) ) {
            update_post_meta(
                $post_id,
                'test_total_marks',
                $_POST['test_total_marks']
            );
        }
        if ( array_key_exists( 'test_programid', $_POST ) ) {
            update_post_meta(
                $post_id,
                'test_programid',
                $_POST['test_programid']
            );
        }
        if ( array_key_exists( 'test_total_marks', $_POST ) ) {
            update_post_meta(
                $post_id,
                'test_total_marks',
                $_POST['test_total_marks']
            );
        }
        if ( array_key_exists( 'assign_students', $_POST ) ) {
                $students = implode(",",$_POST['assign_students']);
                update_post_meta(
                $post_id,
                'assign_students',
                $students
            );
        }
        if ( array_key_exists( 'mcqs', $_POST ) ) {
                update_post_meta(
                $post_id,
                'mcqs',
                $_POST['mcqs']
            );
        }
        if ( array_key_exists( 'mcqs_time', $_POST ) ) {
                update_post_meta(
                $post_id,
                'mcqs_time',
                $_POST['mcqs_time']
            );
        }
        if ( array_key_exists( 'visualize', $_POST ) ) {
                update_post_meta(
                $post_id,
                'visualize',
                $_POST['visualize']
            );
        }
        if ( array_key_exists( 'visualize_time', $_POST ) ) {
                update_post_meta(
                $post_id,
                'visualize_time',
                $_POST['visualize_time']
            );
        }
        if ( array_key_exists( 'grammer', $_POST ) ) {
                update_post_meta(
                $post_id,
                'grammer',
                $_POST['grammer']
            );
        }
        if ( array_key_exists( 'grammer_time', $_POST ) ) {
                update_post_meta(
                $post_id,
                'grammer_time',
                $_POST['grammer_time']
            );
        }
        if ( array_key_exists( 'mcqs_category_id', $_POST ) ) {
                update_post_meta(
                $post_id,
                'mcqs_category_id',
                $_POST['mcqs_category_id']
            );
        }
        if ( array_key_exists( 'visual_category_select', $_POST ) ) {
                update_post_meta(
                $post_id,
                'visual_category_select',
                $_POST['visual_category_select']
            );
        }
        if ( array_key_exists( 'visual_category_id', $_POST ) ) {
                update_post_meta(
                $post_id,
                'visual_category_id',
                $_POST['visual_category_id']
            );
        }
        if ( array_key_exists( 'level1_id', $_POST ) ) {
                update_post_meta(
                $post_id,
                'level1_id',
                $_POST['level1_id']
            );
        }
        if ( array_key_exists( 'level2_id', $_POST ) ) {
                update_post_meta(
                $post_id,
                'level2_id',
                $_POST['level2_id']
            );
        }
        if ( array_key_exists( 'level3_id', $_POST ) ) {
                update_post_meta(
                $post_id,
                'level3_id',
                $_POST['level3_id']
            );
        }
        if ( array_key_exists( 'level4_id', $_POST ) ) {
                update_post_meta(
                $post_id,
                'level4_id',
                $_POST['level4_id']
            );
        }
            if ( array_key_exists( 'level5_id', $_POST ) ) {
                update_post_meta(
                $post_id,
                'level5_id',
                $_POST['level5_id']
            );
        }
        if ( array_key_exists( 'level6_id', $_POST ) ) {
                update_post_meta(
                $post_id,
                'level6_id',
                $_POST['level6_id']
            );
        }
        if ( array_key_exists( 'level7_id', $_POST ) ) {
                update_post_meta(
                $post_id,
                'level7_id',
                $_POST['level7_id']
            );
        }
        if ( array_key_exists( 'level8_id', $_POST ) ) {
                update_post_meta(
                $post_id,
                'level8_id',
                $_POST['level8_id']
            );
        }

    }


    /**
     * Display the meta box HTML to the user.
     *
     * @param \WP_Post $post   Post object.
     */
    public static function html( $post ) {


        $posts_programs = get_posts([
            'post_type' => 'Programs',
            'post_status' => 'publish',
            'numberposts' => -1
            // 'order'    => 'ASC'
        ]);
        $posts_category = get_posts([
            'post_type' => 'question_category',
            'post_status' => 'publish',
            'numberposts' => -1
            // 'order'    => 'ASC'
        ]);
//        echo '<pre>';
//        var_dump($posts_programs);
//        echo '</pre>';
        $test_timelimit = get_post_meta( $post->ID, 'test_timelimit', true );
        $test_passinglevel = get_post_meta( $post->ID, 'test_passinglevel', true );
        $test_programid = get_post_meta( $post->ID, 'test_programid', true );
        $mcqs_category_id = get_post_meta( $post->ID, 'mcqs_category_id', true );
        $visual_category_select = get_post_meta( $post->ID, 'visual_category_select', true );
        $visual_category_id = get_post_meta( $post->ID, 'visual_category_id', true );
        $level1_id = get_post_meta( $post->ID, 'level1_id', true );
        $level2_id = get_post_meta( $post->ID, 'level2_id', true );
        $level3_id = get_post_meta( $post->ID, 'level3_id', true );
        $level4_id = get_post_meta( $post->ID, 'level4_id', true );
        $level5_id = get_post_meta( $post->ID, 'level5_id', true );
        $level6_id = get_post_meta( $post->ID, 'level6_id', true );
        $level7_id = get_post_meta( $post->ID, 'level7_id', true );
        $level8_id = get_post_meta( $post->ID, 'level8_id', true );
        $test_total_marks = get_post_meta( $post->ID, 'test_total_marks', true );
        $get_assign_students = get_post_meta( $post->ID, 'assign_students', true );
        $get_assign_students = explode(',',$get_assign_students);
        $mcqs = get_post_meta( $post->ID, 'mcqs', true );
        $mcqs_time = get_post_meta( $post->ID, 'mcqs_time', true );
        $visualize = get_post_meta( $post->ID, 'visualize', true );
        $visualize_time = get_post_meta( $post->ID, 'visualize_time', true );
        $grammer = get_post_meta( $post->ID, 'grammer', true );
        $grammer_time = get_post_meta( $post->ID, 'grammer_time', true );
        $assign_students = get_users( ['role' => 'student']);
        ?>

        <div class="row">

            <div class="col-md-12" >
                <label for="wporg_field">Select Program Type</label>
                <select name="test_programid" id="test_programid" class="form-control" required>

                    <option value="">Select Program</option>
                    <?php
                    foreach($posts_programs as $p){

                        ?>
                        <option value="<?php echo $p->ID ?>"  <?php selected( $test_programid, $p->ID ); ?>><?php echo $p->post_title ?></option>
                        <?php

                    }

                    ?>
                </select>
            </div>
<!--            <div class="col-md-12">-->
<!--                <label for="test_timelimit">Time Limit (in mins)</label>-->
<!--                <input type="number" id="test_timelimit"  class="form-control" name="test_timelimit"  value="--><?php //if(isset($test_timelimit)){ echo $test_timelimit;  }  ?><!--" min="1"  required>-->
<!--            </div>-->
<!--            <div class="col-md-12">-->
<!--                <label for="test_timelimit">Passing level</label>-->
<!--                <input type="number" id="test_passinglevel"  class="form-control" name="test_passinglevel"  value="--><?php //if(isset($test_passinglevel)){ echo $test_passinglevel;  }  ?><!--" min="1" required>-->
<!--            </div>-->
<!--            <div class="col-md-12">-->
<!--                <label for="test_total_marks">Total Marks</label>-->
<!--                <input type="number" id="test_total_marks"  class="form-control" name="test_total_marks"  value="--><?php //if(isset($test_total_marks)){ echo $test_total_marks;  }  ?><!--" min="1" required>-->
<!--            </div>-->
<!--          -->
            <div class="col-md-12">
                <label for="assign_students">Select Student</label>
                <select name="assign_students[]" id="assign_students" class="form-control select2" required multiple>
                    <?php
                    foreach($assign_students as $a){

                        ?>
                        <option value="<?php echo $a->ID ?>"  <?php if(in_array($a->ID,$get_assign_students)) { echo 'selected'; } ?>><?php echo $a->user_nicename ?></option>
                        <?php

                    }

                    ?>
                </select>
            </div><br>
            <div class="col-md-12 mcq">
                    <div class="mcq_category">
                    <label for="mcqs">MCQs</label>
                        <input type="checkbox" id="mcqs" name="mcqs" value="1" <?php echo ($mcqs == 1 ? 'checked' : '');?> >
                        <input type="text" id="mcqs_time" name="mcqs_time" style="display:none" placeholder="Mcqs Time" value="<?php if(isset($mcqs_time)){ echo $mcqs_time;  }  ?>">          
                        <select name="mcqs_category_id" id="mcqs_category_id" class="form-control" style="width:200px !important">
                            <option value="">Select Category</option>
                            <?php foreach($posts_category as $p){ ?>
                                <option value="<?php echo $p->ID ?>"  <?php selected( $mcqs_category_id, $p->ID ); ?>><?php echo $p->post_title ?></option>
                                <?php } ?>
                        </select>
                    </div>
              </div>
            <div class="col-md-12 mcq">
                <div class="mcq_category">
                    <label for="visualize">Visualize</label>
                    <input type="checkbox" id="visualize" name="visualize" value="1" <?php echo ($visualize == 1 ? 'checked' : '');?> >
                    <input type="text" id="visualize_time" name="visualize_time" style="display:none" placeholder="Visualize Time" value="<?php if(isset($visualize_time)){ echo $visualize_time;  }  ?>">
                    <select name="visual_category_select" id="visual_category_select" class="form-control"  style="width:200px !important">
                    <option value="">Select Category</option>
                    <option value="level" <?php selected( $visual_category_select, 'level' ); ?>>Level Wise</option>
                    <option value="category" <?php selected( $visual_category_select, 'category' ); ?>>Category Wise</option>
                    </select>
                    <select name="visual_category_id" id="visual_category_id" class="form-control"  style="width:200px !important; display:none">
                        <option value="">Select Category</option>
                        <?php foreach($posts_category as $p){ ?>
                        <option value="<?php echo $p->ID ?>"  <?php selected( $visual_category_id, $p->ID ); ?>><?php echo $p->post_title ?></option>
                        <?php } ?>
                    </select>
                    </div>
                    <div id="level">
                        <div class="data">
                             <label for="Level 1">Level 1</label>
                             <select name="level1_id" id="level1_id" class="form-control "  style="width:200px !important;">
                                <option value="">Select Category</option>
                                <?php foreach($posts_category as $p){ ?>
                                <option value="<?php echo $p->ID ?>"  <?php selected( $level1_id, $p->ID ); ?>><?php echo $p->post_title ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="data">
                           <label for="Level 2">Level 2</label>
                           <select name="level2_id" id="level2_id" class="form-control "  style="width:200px !important;">
                                <option value="">Select Category</option>
                                <?php foreach($posts_category as $p){ ?>
                                <option value="<?php echo $p->ID ?>"  <?php selected( $level2_id, $p->ID ); ?>><?php echo $p->post_title ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="data">
                           <label for="Level 3">Level 3</label>
                           <select name="level3_id" id="level3_id" class="form-control "  style="width:200px !important;">
                                <option value="">Select Category</option>
                                <?php foreach($posts_category as $p){ ?>
                                <option value="<?php echo $p->ID ?>"  <?php selected( $level3_id, $p->ID ); ?>><?php echo $p->post_title ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="data">
                           <label for="Level 4">Level 4</label>
                           <select name="level4_id" id="level4_id" class="form-control "  style="width:200px !important;">
                                <option value="">Select Category</option>
                                <?php foreach($posts_category as $p){ ?>
                                <option value="<?php echo $p->ID ?>"  <?php selected( $level4_id, $p->ID ); ?>><?php echo $p->post_title ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="data">
                           <label for="Level 5">Level 5</label>
                           <select name="level5_id" id="level5_id" class="form-control "  style="width:200px !important;">
                                <option value="">Select Category</option>
                                <?php foreach($posts_category as $p){ ?>
                                <option value="<?php echo $p->ID ?>"  <?php selected( $level5_id, $p->ID ); ?>><?php echo $p->post_title ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="data">
                           <label for="Level 6">Level 6</label>
                           <select name="level6_id" id="level6_id" class="form-control "  style="width:200px !important;">
                                <option value="">Select Category</option>
                                <?php foreach($posts_category as $p){ ?>
                                <option value="<?php echo $p->ID ?>"  <?php selected( $level6_id, $p->ID ); ?>><?php echo $p->post_title ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="data">
                           <label for="Level 7">Level 7</label>
                           <select name="level7_id" id="level7_id" class="form-control "  style="width:200px !important;">
                                <option value="">Select Category</option>
                                <?php foreach($posts_category as $p){ ?>
                                <option value="<?php echo $p->ID ?>"  <?php selected( $level7_id, $p->ID ); ?>><?php echo $p->post_title ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="data">
                           <label for="Level 8">Level 8</label>
                           <select name="level8_id" id="level8_id" class="form-control "  style="width:200px !important;">
                                <option value="">Select Category</option>
                                <?php foreach($posts_category as $p){ ?>
                                <option value="<?php echo $p->ID ?>"  <?php selected( $level8_id, $p->ID ); ?>><?php echo $p->post_title ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
            </div><br>

        </div>
        <style>
            .mcq{
                margin-bottom:20px;
            }
            .mcq_category{
              display:inline;
            }
            .mcq_category select{
                margin:0px !important;
            }
            #level{
                margin-top:20px;
            }
            #level label{
                font-weight:bold;
                padding: 30px;
            }
            .hide{
              display:none;
            }
        </style>
        <script>
            jQuery(document).ready(function($) {
                if ($('#mcqs').prop('checked')) {
                    $('#mcqs_time').show();
                    $('#mcqs_category_id').show();
                }
                if($('#visualize').prop('checked')){
                    $('#visualize_time').show();
                    $('#visual_category_select').show();
                }
                if($('#visual_category_select').val() == 'level'){
                    $('#level').show();
                    $('#visual_category_id').hide();
                }else if($(this).val() == 'category'){
                    $('#level').hide();
                    $('#visual_category_id').show();
                }else{
                    $('#level').hide();
                    $('#visual_category_id').hide();
                }


                $('#mcqs').on('click', function () {
                    if ($(this).prop('checked')) {
                        $('#mcqs_time').show();
                        $('#mcqs_category_id').show();
                    }else{
                        $('#mcqs_time').hide();
                        $('#mcqs_category_id').hide();
                    } 
                });
                $('#visualize').on('click', function () {
                    if ($(this).prop('checked')) {
                        $('#visualize_time').show();
                        $('#visual_category_select').show();
                    }else{
                        $('#level').hide();
                        $('#visualize_time').hide();
                        $('#visual_category_select').hide();
                    } 
                });
                $('#visual_category_select').on('change', function () {
                    if ($(this).val() == 'level') {
                        $('#level').show();
                        $('#visual_category_id').val('');
                        $('#visual_category_id').hide();
                    }else if($(this).val() == 'category'){
                        $('#level1_id').val('');
                        $('#level2_id').val('');
                        $('#level3_id').val('');
                        $('#level4_id').val('');
                        $('#level5_id').val('');
                        $('#level6_id').val('');
                        $('#level7_id').val('');
                        $('#level8_id').val('');
                        $('#level').hide();
                        $('#visual_category_id').show();
                    }else{
                        $('#level').hide();
                        $('#visual_category_id').hide();
                    }
                });


                jQuery('.select2').select2();
            });
        </script>
        <?php
    }
}

add_action( 'add_meta_boxes', [ 'Vgc_Test_Meta_Box', 'add' ] );
add_action( 'save_post', [ 'Vgc_Test_Meta_Box', 'save' ] );
