<?php
add_shortcode('vgc_login_agent', 'wpagent_login');
function wpagent_login()
{
    if(!is_user_logged_in()){
        ob_start();
        echo '<style>
            p.error {
                color: red;
                margin-bottom: -41px;
            }
            </style>';

        $login  = (isset($_GET['login']) ) ? $_GET['login'] : 0;
        if ($login === "failed") {

            echo '<p class="error"><strong>ERROR:</strong> Invalid username and/or password.</p><br><br>';
        } elseif ($login === "empty") {
            echo '<p class="error"><strong>ERROR:</strong> Username or Password is empty.</p><br><br>';
        } elseif ($login === "false") {
            echo '<p class="success"> You are logged out now.</p><br><br>';
        }
        echo '<h2 class="student_login"> Sign In </h2>';
        $args = array(
            'redirect' => '',
            'form_id' => 'loginform-custom',
            'label_username' => __( 'Username' ),
            'label_password' => __( 'Password' ),
            'label_remember' => __( 'Remember Me' ),
            'label_log_in' => __( 'Log In' ),
            'remember' => true
        );



        return wp_login_form($args);

    }else{
        return '<div class="alert alert-info">
    User Already Loggedin!
</div>';

    }
//    $msg = '';
//    if (isset($_REQUEST['submit_vgc_agent_login_page'])) {
//        $password = $_POST['password'];
//        $email = $_POST['email'];
//        if (email_exists($email) == true) {
//            // Get current user object
//            $user = get_user_by('email', $email);
//            if(wp_check_password( $password,$user->data->user_pass, $user->data->ID )){
//                if($user->roles[0] == 'agent'){
//                    $user_id = $user->data->ID;
//                    clean_user_cache($user_id);
//                    wp_set_current_user($user_id);
//                   dd( wp_set_auth_cookie($user_id, true));
//                    $user = get_user_by('id', $user_id);
//                    update_user_caches($user);
//                    return home_url();
//                     $msg = '<p style="color : green">Agent Login Successfully!</p>';
//                }else{
//                    $msg = '<p style="color : green">Access Denied</p>';
//                }
//            }else{
//                $msg = '<p style="color : red">ID or Password Mismatch</p>';
//            }
//        } else {
//            $msg = '<p style="color : red">Agent Not Exist</p>';
//        }
//
//
//    }
//    $vgc_student_reg_page_id = get_option("vgc_student_reg_page_id");
//    if(!is_user_logged_in()){
//    $html = '<div class="plugin_form">
//        <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
//          <div class="form_main_div">
//            <input type="email" name="email" class="plugin_form_input"  placeholder="Enter Email"   required>
//            <input type="password" name="password" class="plugin_form_input"  placeholder="Enter Password"   required>
//          </div>
//          <button type="submit" name="submit_vgc_agent_login_page" class="submitbtn">SUBMIT</button>
//          ' . $msg . '
//        </form>
//      </div>';
//
//        }
//    else{
//
//        $html = '<div class="alert alert-info">
//    User Already Loggedin!
//</div>';
//
//
//    }
//
//    return $html;
}
function my_front_end_login_fail( $username ) {
    $referrer = $_SERVER['HTTP_REFERER'];  // where did the post submission come from?
    // if there's a valid referrer, and it's not the default log-in screen
    if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
        $key = 'login';
        $url = $referrer;

// Remove specific parameter from query string
        $referrer = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);
        wp_redirect( $referrer . '?login=failed' );  // let's append some information (login=failed) to the URL for the theme to use
        exit;
    }
}

add_action( 'wp_login_failed', 'my_front_end_login_fail' );  // hook failed login



function damn_login_blank( $user ) {

    if ( isset( $_SERVER['HTTP_REFERER'] ) ) {

        $referrer = $_SERVER['HTTP_REFERER'];
    }
    else {

        $referrer = '';
    }

    $error = false;

    //Check your username and password inputs - these might be different if you changed them - 'log' is the name attribute of username and 'pwd' for password, just change to whatever you have
    if( isset( $_POST['log'] ) && $_POST['log'] == '' || isset( $_POST['pwd'] ) && $_POST['pwd'] == '' ) {

        $error = true;
    }

    //Check that we're not on the default login page
    if ( ! empty( $referrer ) && ! strstr( $referrer,'wp-login' ) && ! strstr( $referrer, 'wp-admin' ) && $error ) {
        $key = 'login';
        $url = $referrer;

// Remove specific parameter from query string
        $referrer = preg_replace('~(\?|&)'.$key.'=[^&]*~', '$1', $url);
        //Make sure we don't already have a failed login attempt to prevent double query string
        if ( ! strstr( $referrer, '?login=empty' ) ) {


            //Redirect to CURRENT page and append a querystring of login failed
            wp_redirect( $referrer . '?login=empty' );
        }
        else {


            wp_redirect( $referrer );
        }

        exit;
    }
}
add_action( 'authenticate', 'damn_login_blank' );

function customloginplaceholder (){

    echo '<script>jQuery(document).ready(function() {
  jQuery("input#user_login").attr("placeholder", "Username or email");  
     jQuery("input#user_pass").attr("placeholder", "Password");
});</script>';




}

add_action('wp_footer', 'customloginplaceholder');

