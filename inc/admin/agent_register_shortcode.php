<?php
add_shortcode('vgc_register_agent', 'wpagent_register');
function wpagent_register()
{
    $msg = '';
    if (isset($_REQUEST['submit_vgc_agent_reg_page'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $email = $_POST['email'];
        if (email_exists($email) == false && username_exists($username) == null) {

            // Create the new user
            $user_id = wp_create_user($username, $password, $email);

            // Get current user object
            $user = get_user_by('id', $user_id);
            // Remove role
            $user->remove_role('subscriber');
            // Add role
            $user->add_role('agent');
            update_user_meta($user_id, 'cp_date', $_POST['cp_date']);
            update_user_meta($user_id, 'cp_subject', null);
            update_user_meta($user_id, 'cp_location', $_POST['cp_location']);
            update_user_meta($user_id, 'cp_grade', '');
            update_user_meta($user_id, 'cp_student_number', 'vgc-' . $user_id);
            $msg = '<p style="color : green">Agent Registered Successfully!</p>';
        } else {


            $msg = '<p style="color : red">username/email already exist!</p>';


        }


    }
    $vgc_student_reg_page_id = get_option("vgc_student_reg_page_id");

    if(!is_user_logged_in()) {
        $html = '<div class="plugin_form">
        <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
          <div class="form_main_div">
            <input type="text" name="username" class="plugin_form_input"  placeholder="Enter UserName"   required>
            <input type="email" name="email" class="plugin_form_input"  placeholder="Enter Email"   required>
            <input type="password" name="password" class="plugin_form_input"  placeholder="Enter Password"   required>
            <input type="date" name="cp_date" class="plugin_form_input"  placeholder="Enter Date"   required>
            <input type="text" name="cp_location" class="plugin_form_input"  placeholder="Enter Location"   required>
           
        
          </div>
          <button type="submit" name="submit_vgc_agent_reg_page" class="submitbtn">SUBMIT</button>
          ' . $msg . '
        </form>
      </div>';

    }

else{

    $html = '<div class="alert alert-info">
    User Already Loggedin!
</div>';


}

    return $html;
}