<?php

function vgc_get_user_field_name($field_name)
{
    $user_query =  get_users();
    $data = [];
    // User Loop
    if (!empty($user_query)) {
        foreach ($user_query as $users) {


            $field = esc_attr(get_the_author_meta($field_name, $users->ID));

            if ($field) {
                array_push($data,$field);
            }
        }  
    }
    return $data;
}

function vgc_add_user_section_filter()
{
    
    $selected_subject = $_GET['cp_subject'] ?? null;
    $selected_location = $_GET['cp_location'] ?? null;
    $selected_dob = $_GET['cp_date'] ?? null;

    echo ' <select name="cp_subject[]" style="float:none;"><option value="">Subject...</option>';
    $data_subject = vgc_get_user_field_name('cp_subject');
    foreach ($data_subject as $i) {
        $selected = $i == $selected_subject ? ' selected="selected"' : '';
        echo '<option value="' . $i . '"' . $selected . '> ' . $i . '</option>';
    }
    echo '</select>';

   
    echo ' <select name="cp_location[]" style="float:none;"><option value="">Location...</option>';
    $data_location = vgc_get_user_field_name('cp_location');
    foreach ($data_location as $i) {
        $selected = $i == $selected_location ? ' selected="selected"' : '';
        echo '<option value="' . $i . '"' . $selected . '> ' . $i . '</option>';
    }
    echo '</select>';
    echo '<label for="date">Select Date</label>';
    echo ' <input type="date" name="cp_date[]" value="'.$selected_dob[0].'" id="date" data-provide="datepicker">';
    echo '<input type="submit" class="button" value="Filter">';
}
add_action('restrict_manage_users', 'vgc_add_user_section_filter');
function vgc_filter_users_section($query)
{
    global $pagenow;

    $selected_filters = [];


    if (is_admin() && 'users.php' == $pagenow) {
        $selected_subject = $_GET['cp_subject'] ??  null;
        $selected_location = $_GET['cp_location'] ??  null;
        $selected_dob = $_GET['cp_date'] ??  null;
        if(null !== $selected_subject){
            $selected_filters = array(
                'key' => 'cp_subject',
                'value' => $selected_subject
            );
        }if(null !== $selected_location){
            $selected_filters = array(
                'key' => 'cp_location',
                'value' => $selected_location
            );
        }if(null !== $selected_dob){
            $selected_filters = array(
                'key' => 'cp_date',
                'value' => $selected_dob
            );
        }

        if (!empty($selected_filters)) {
            $meta_query = array(
               $selected_filters
            );
            
            $query->set('meta_query', $meta_query);
        }
    }
}
add_filter('pre_get_users', 'vgc_filter_users_section');
