<?php


function create_role()
{
    add_role(
        'agent',
        'Agent',
    );
    add_role(
        'student',
        'Student',
    );
}


// Add the simple_role.
add_action('init', 'create_role');


add_filter( 'wp_nav_menu_items', 'your_custom_menu_item', 10, 2 );
function your_custom_menu_item ( $items, $args ) {

    if ($args->theme_location == 'primary') {
        if (is_user_logged_in()) {
            $items .= '<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-789"> <a href="' . wp_logout_url() . '">Log Out</a></li>';
        }
    }
    //echo $items;

    return $items;
}
add_action('wp_logout','ps_redirect_after_logout');
function ps_redirect_after_logout(){
    wp_redirect( home_url() );
    exit();
}

//
//add_action('init', 'create_pages');
//function create_pages()
//{
//    $msg = '';
//    $vgc_student_reg_page_id = get_option("Vgc_dashboard");
//
//  $html = '<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
//  <li class="nav-item">
//    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Home</a>
//  </li>
//  <li class="nav-item">
//    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Profile</a>
//  </li>
//  <li class="nav-item">
//    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Contact</a>
//  </li>
//</ul>
//<div class="tab-content" id="pills-tabContent">
//  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">...</div>
//  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">...</div>
//  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">...</div>
//</div>';
//
//
//    if (!$vgc_student_reg_page_id) {
////create a new page and automatically assign the page template
//        $post1 = array(
//            'post_title' => "Vgc Dashboard",
//            'post_content' => $html,
//            'post_status' => "publish",
//            'post_type' => 'page',
//        );
//        $postID = wp_insert_post($post1);
//        update_post_meta($postID, "_wp_page_template", "vgc_student_reg_page.php");
//        update_option("vgc_student_reg_page_id", $postID);
//    } else {
//        wp_update_post(array(
//            'ID' => $vgc_student_reg_page_id,
//            'post_title' => "Vgc Dashboard",
//            'post_content' => $html,
//            'post_status' => 'publish'
//        ));
//    }
//}
//
