<?php


function my_custom_post_program() {

//labels array added inside the function and precedes args array

    $labels = array(
        'name' => _x( 'Programs', 'post type general name' ),
        'singular_name' => _x( 'Programs', 'post type singular name' ),
        'add_new' => _x( 'Add New', 'Vgc Programs' ),
        'add_new_item' => __( 'Add New Program' ),
        'edit_item' => __( 'Edit Program' ),
        'new_item' => __( 'New Program' ),
        'all_items' => __( 'All Program' ),
        'view_item' => __( 'View Program' ),
        'search_items' => __( 'Search Program' ),
        'not_found' => __( 'No Program found' ),
        'not_found_in_trash' => __( 'No Program found in the Trash' ),
        'parent_item_colon' => '',
        'menu_name' => 'VGC Programs'
    );

// args array

    $args = array(
        'labels' => $labels,
        'description' => 'Create,update,delete of programs',
        'public' => true,
        'menu_position' => 4,
        'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
        'has_archive' => true,
    );

    register_post_type( 'Programs', $args );
}


add_action( 'init', 'my_custom_post_program' );




