<?php


function my_custom_post_question() {

//labels array added inside the function and precedes args array

    $labels = array(
        'name' => _x( 'questions', 'post type general name' ),
        'singular_name' => _x( 'questions', 'post type singular name' ),
        'add_new' => _x( 'Add New', 'Vgc Tests' ),
        'add_new_item' => __( 'Add New Question' ),
        'edit_item' => __( 'Edit Question' ),
        'new_item' => __( 'New Question' ),
        'all_items' => __( 'All Question' ),
        'view_item' => __( 'View Question' ),
        'search_items' => __( 'Search Question' ),
        'not_found' => __( 'No Question found' ),
        'not_found_in_trash' => __( 'No Question found in the Trash' ),
        'parent_item_colon' => '',
        'menu_name' => 'VGC Questions'
    );



    $args = array(
        'labels' => $labels,
        'description' => 'Create,update,delete of questions',
        'public' => true,
        'menu_position' => 4,
        'supports' => array( 'title' ),
        'has_archive' => true,
    );

    register_post_type( 'questions', $args );
}


add_action( 'init', 'my_custom_post_question' );

require  'Vgc_Question_Meta_Box.php';


