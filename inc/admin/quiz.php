<?php

add_filter( 'page_template', 'wpa3396_page_template' );
function wpa3396_page_template( $page_template )
{
    if ( is_page( 'user-account' ) ) {      
      $page_template =  plugin_dir_path( __DIR__ ). '/user/user_account.php';
    }
    if ( is_page( 'quiz-test' ) ) {      
      $page_template =  plugin_dir_path( __DIR__ ). '/user/quiz_test.php';
    }
        if ( is_page( 'video-quiz-test' ) ) {
        $page_template =  plugin_dir_path( __DIR__ ). '/user/videoquiz.php';
    }
    if ( is_page( 'student-check-result' ) ) {

        $page_template =  plugin_dir_path( __DIR__ ). '/user/checkresult.php';
    }
    if ( is_page( 'check-quiz-setting' ) ) {
        $page_template =  plugin_dir_path( __DIR__ ). '/user/checkquizsettings.php';
    }
    if ( is_page( 'ans-quiz-test' ) ) {
      $page_template =  plugin_dir_path( __DIR__ ). '/user/ans_quiz.php';
    } 

    return $page_template;
}

function hcf_register_meta_boxes() {
  add_meta_box( 'hcf-1', __( 'Question Answers', 'hcf' ), 'hcf_display_callback', 'quizresult' );
  add_meta_box( 'hcf-2', __( 'Result', 'hcf2' ), 'hcf_display_result', 'quizresult' );
}
add_action( 'add_meta_boxes', 'hcf_register_meta_boxes' );

function hcf_display_result( $post ) {
  
  $percentage     = get_post_meta($post->ID , 'percentage' , true);
  $playerscore    = get_post_meta($post->ID , 'playerscore' , true );
  $question_count = get_post_meta($post->ID , 'question_count' , true );
  $total_count = get_post_meta($post->ID , 'question_count', true);
  $mcqcorrectanscount = 0;
  $mcqscount = 0;
  $videocount = 0;
  $videocorrectcount = 0;

    for($i= 1 ; $i<= $total_count ; $i++) {
        $data = get_post_meta($post->ID, 'question_' . $i, true);
        if($data['type'] == 'mcqs') {
            $mcqscount += 1;
            if ($data['answer'] == $data['correct_ans']) {

                $mcqcorrectanscount += 1;


            }
        }
        if($data['type'] == 'video') {
            $videocount += 1;
            if ($data['answer'] == 'correct') {

                $videocorrectcount += 1;


            }
        }
    }
    $level= 'level-0';
    if($mcqcorrectanscount <= 9){
        $level= 'level-2';
    }
    elseif($mcqcorrectanscount > 9 && $mcqcorrectanscount <= 21){
        $level= 'level-3';
    }
    elseif($mcqcorrectanscount > 21 && $mcqcorrectanscount <= 32){
        $level= 'level-4';
    }
    elseif($mcqcorrectanscount > 33 && $mcqcorrectanscount <= 45){
        $level= 'level-5';
    }
    elseif($mcqcorrectanscount > 46 && $mcqcorrectanscount <= 58){
        $level= 'level-6';
    }
    elseif($mcqcorrectanscount > 58 && $mcqcorrectanscount <= 64){
        $level= 'level-7';
    }
    else{
        $level= 'level-8';
    }
  //var_dump($percentage);
  ?>
  <div class="hcf_box">


  <p class="meta-options hcf_field">
      <label for="correct_answer"><b>MCQS Total Questions</b></label>
      <input id="correct_answer" type="text" value="<?= $mcqscount ?>" name="correct_answer">
  </p>
      <p class="meta-options hcf_field">
          <label for="user_answer"><b>MCQS Score</b></label>
          <input id="user_answer" type="text" value="<?= $mcqcorrectanscount ?>" name="user_answer">
      </p>

      <p class="meta-options hcf_field">
          <label for="user_answer"><b>Student Level</b></label>
          <input id="user_answer" type="text" value="<?= $level ?>" name="level">
      </p>

      <p class="meta-options hcf_field">
          <label for="correct_answer"><b>Video Total Questions</b></label>
          <input id="correct_answer" type="text" value="<?= $videocount ?>" name="correct_answer">
      </p>
      <p class="meta-options hcf_field">
          <label for="user_answer"><b>Video Score</b></label>
          <input id="user_answer" type="text" value="<?= $videocorrectcount ?>" name="user_answer">
      </p>
      <p class="meta-options hcf_field">
          <label for="question"><b>Percentage</b></label>
          <input id="question" type="text" value="<?= ($percentage != '') ? number_format($percentage, 2) : '' ?>" name="question">
      </p>
</div>
<?php
}

/**
* Meta box display callback.
*
* @param WP_Post $post Current post object.
*/
function hcf_display_callback( $post ) { ?>
  <style scoped>
      .hcf_box{
          display: grid;
          grid-template-columns: max-content 1fr;
          grid-row-gap: 10px;
          grid-column-gap: 20px;
      }
      .hcf_field{
          display: contents;
      }
  </style>
<?php
$total_count = get_post_meta($post->ID , 'question_count', true);
for($i= 1 ; $i<= $total_count ; $i++){ 
  $data =  get_post_meta($post->ID , 'question_'.$i, true );
  
 //var_dump($data);
  ?>
  <div class="hcf_box">
  <p class="meta-options hcf_field">
      <label for="question"><b>Question <?= $i?></b></label>
      <input id="question" type="text" value="<?= $data['question'] ?>" name="question">
  </p>
  <?php
  if($data['type']  == 'video'){
  ?>
  <p class="meta-options hcf_field">
      <label for="user_answer">User Answer</label>
      <video width="320" height="240" controls>
  <source src="<?= $data['answer'] ?>" type="video/mp4">

</video>
  </p>
  
  <?php }elseif($data['type']  == 'grammer'){ ?>
    <p class="meta-options hcf_field">
    <label for="user_answer">User Answer</label>
    <input id="user_answer" type="text" value="<?= $data['answer'] ?>" name="user_answer">
  </p>
 <?php } else{ ?>
  <p class="meta-options hcf_field">
      <label for="user_answer">User Answer</label>
      <input id="user_answer" type="text" value="<?= $data['answer'] ?>" name="user_answer">
  </p>
  <p class="meta-options hcf_field">
      <label for="correct_answer">Correct Answer</label>
      <input id="correct_answer" type="text" value="<?= $data['correct_ans'] ?>" name="correct_answer">
  </p>
  
    <?php } ?>
  
</div>
<hr>
<?php } ?>

<?php 
}

add_action("wp_ajax_my_quiz_test", "my_quiz_test");
add_action("wp_ajax_nopriv_my_quiz_test", "my_quiz_test");

function my_quiz_test() {

        $quiz_id = $_POST['quiz_id'];
      $user_id = get_current_user_id();
      $args = array(
        'numberposts'   => -1,
        'post_type'     => 'quizresult',
        'meta_query'    => array(
            'relation'      => 'AND',
            array(
                'key'       => 'quiz_id',
                'compare'   => '=',
                'value'     => $quiz_id,
            ),
            array(
              'key'       => 'user',
              'compare'   => '=',
              'value'     => $user_id,
          )
        )
    );
    $get_result = get_posts($args);

    if(empty($get_result) == true){
        $user = get_user_by('id', $user_id);
        $data = array('post_title'    => 'Quiz By '.$user->data->user_login.' Quiz Id: '.$quiz_id,
        'post_status'   => 'publish',
        'post_type'     => 'quizresult'
        );

        $result_id = wp_insert_post( $data );
        update_post_meta($result_id , 'quiz_id',$quiz_id);
        update_post_meta($result_id , 'user',$user_id);
        $url = get_site_url().'/quiz-test/?quiz='.$quiz_id.'';
        $data = array("type"=>'success', "result_id"=>$result_id,'url' => $url );
        echo json_encode($data);
    }
    elseif(empty($get_result) == false){

      $percentage = get_post_meta($get_result[0]->ID , 'percentage' ,true );
      $mcqs_end = get_post_meta($get_result[0]->ID , 'mcqs_end' ,true );
      $video_end = get_post_meta($get_result[0]->ID , 'video_end' ,true );
      if(!empty($mcqs_end) && !empty($video_end)){


          $url = get_site_url().'/user-account/?msg=quiz-complete';
      }

      elseif(!empty($mcqs_end) && empty($video_end)){


          $url = get_site_url().'/video-quiz-test/?quiz='.$quiz_id.'';

      }
      elseif(empty($mcqs_end) && !empty($video_end)){

          $url = get_site_url().'/quiz-test/?quiz='.$quiz_id.'';


      }
      else{


          $url = get_site_url().'/quiz-test/?quiz='.$quiz_id.'';

      }

        $data = array("type"=>'success' , 'url' => $url);
        echo json_encode($data);

    }
    else{
        $url = get_site_url().'/user-account/?msg=quiz-complete';
     $data = array("type"=>'failed', "result_id"=>0, 'url' => $url);
      echo json_encode($data);
    }



   die();

}

add_action("wp_ajax_get_time_data", "get_time_data");
add_action("wp_ajax_nopriv_get_time_data", "get_time_data");

function get_time_data() {
    $quiz_id = $_POST['quiz_id'];
    $time = $_POST['time'];
    $user_id = get_current_user_id();
    $args = array(
      'numberposts'   => -1,
      'post_type'     => 'quizresult',
      'meta_query'    => array(
          'relation'      => 'AND',
          array(
              'key'       => 'quiz_id',
              'compare'   => '=',
              'value'     => $quiz_id,
          ),
          array(
            'key'       => 'user',
            'compare'   => '=',
            'value'     => $user_id,
        )
      )
  );
    $result_id = get_posts($args);

    update_post_meta($result_id[0]->ID , 'time' , $time);
    $data = array("time" =>$time);
    echo json_encode($data);
    die();
}

add_action("wp_ajax_save_result_res", "save_result_res");
add_action("wp_ajax_nopriv_save_result_res", "save_result_res");

function save_result_res() {
  $quiz_id = $_POST['quiz_id'];
  $answer = $_POST['answer'];
  $question = $_POST['question'];
  $question_id = $_POST['question_id'];
  $correct_ans = $_POST['correct_ans'];
  $type = $_POST['type'];
  $time = $_POST['time'];
  $is_quiz = $_POST['is_quiz'];
  $user_id = get_current_user_id();
  $args = array(
    'numberposts'   => -1,
    'post_type'     => 'quizresult',
    'meta_query'    => array(
        'relation'      => 'AND',
        array(
            'key'       => 'quiz_id',
            'compare'   => '=',
            'value'     => $quiz_id,
        ),
        array(
          'key'       => 'user',
          'compare'   => '=',
          'value'     => $user_id,
      )
    )
);
  $result_id = get_posts($args);
  $questions = get_post_meta( $result_id[0]->ID, 'question_ids', true );
  $correct_count = get_post_meta( $result_id[0]->ID, 'correct_count', true );
	if ( in_array( $question_id, $questions ) ) {
		$questions = array_filter( $questions, function ( $w ) use ( &$question_id ) {
			return $w !== $question_id;
		} );
	} else if($questions == ''){
    $questions = []; 
    $questions[] = $question_id;
  }else{
		$questions[] = $question_id;
	}

  if($type == 'video'){
    $answer = wp_get_attachment_url( $answer );
  }
  if($type == 'mcqs'){
    if($answer == $correct_ans){
        if($correct_count == ''){
          $correct_count = 1;
        }else{
          $correct_count = $correct_count+1;
        }
    }
  }
  $newdata = array (
    'quiz_id' => $quiz_id,
    'question_id' => $question_id,
    'time' => $time,
    'question' =>$question,
    'answer' => $answer,
    'correct_ans' => $correct_ans,
    'type' =>$type,
    'question_no' => $question_no
  );


    $current_q = get_post_meta($result_id[0]->ID , 'question_count' ,true );
   if($current_q == ''){
    $current_q = 1;
   }else{
    $current_q = $current_q+1;
   }
   update_post_meta($result_id[0]->ID , 'question_count' , $current_q );
   update_post_meta($result_id[0]->ID , 'correct_count' , $correct_count );
   update_post_meta($result_id[0]->ID , 'question_'.$current_q , $newdata );
   update_post_meta( $result_id[0]->ID, 'question_ids', $questions );

   $data = array(
     "type"=>'success'
    );
   echo json_encode($data);
   die();

}

add_action("wp_ajax_quiz_end", "quiz_end");
add_action("wp_ajax_nopriv_quiz_end", "quiz_end");

function quiz_end() {
  $quiz_id = $_POST['quiz_id'];
  $playerScore = $_POST['playerScore'];
  $no_q = $_POST['no_q'];
  $playerGrade = $_POST['playerGrade'];
  $complete_time = $_POST['time'];
  $mcqtest = $_POST['mcqtest'];
  $type = $_POST['type'];
  
  $user_id = get_current_user_id();

  $args = array(
    'numberposts'   => -1,
    'post_type'     => 'quizresult',
    'meta_query'    => array(
        'relation'      => 'AND',
        array(
            'key'       => 'quiz_id',
            'compare'   => '=',
            'value'     => $quiz_id,
        ),
        array(
          'key'       => 'user',
          'compare'   => '=',
          'value'     => $user_id,
      )
    )
);
  $result_id = get_posts($args);
 
   update_post_meta($result_id[0]->ID , 'playerscore' , $playerScore );
    if($mcqtest == 1){

      update_post_meta($result_id[0]->ID , 'mcqs_end' , 1 );
      update_post_meta($result_id[0]->ID , 'percentage' , $playerGrade );

    }else{
      update_post_meta($result_id[0]->ID , 'video_end' , 1 );
      update_post_meta($result_id[0]->ID , 'percentage' , $playerGrade );
      update_post_meta($result_id[0]->ID , 'complete_time' , $complete_time );   
    }
  
    if($type == 'mcqs'){
        $check = get_post_meta( $quiz_id, 'finish', true );
      $correct_count = get_post_meta( $result_id, 'correct_count', true );
      $question_count = get_post_meta( $result_id, 'question_count', true );
      $visual_category_select = get_post_meta( $quiz_id, 'visual_category_select', true );
      if($visual_category_select == 'level'){
          if($correct_count <= 9){
          update_post_meta($result_id , 'level' , 'level_1' );   
          }else if($correct_count > 9 && $correct_count <= 21){
            update_post_meta($result_id , 'level' , 'level_2' );
          }else if($correct_count > 21 && $correct_count <= 32){
            update_post_meta($result_id , 'level' , 'level_3' );
          }else if($correct_count > 32 && $correct_count <= 45){
            update_post_meta($result_id , 'level' , 'level_4' );
          }else if($correct_count > 45 && $correct_count <= 58){
            update_post_meta($result_id , 'level' , 'level_5' );
          }else if($correct_count > 58 && $correct_count <= 65){
            update_post_meta($result_id , 'level' , 'level_6' );
          }
        }
        if($check == '1'){
        $url = "check-quiz-setting/?quiz=".$quiz_id;
        }else{
        $url = "user-account/";
        }
   }else if($type == 'video'){
       if($check == '1'){
        $url = "check-quiz-setting/?quiz=".$quiz_id;
        }else{
        $url = "user-account/";
       }
   }else{
       $url = "user-account/";
   }
   $data = array(
     "type"=>'success',
     "url"=> $url
    );
   echo json_encode($data);
   die();

}

add_action("wp_ajax_save_img", "save_img");
add_action("wp_ajax_nopriv_save_img", "save_img");

function base64_to_jpeg($base64_string, $output_file) {
    // open the output file for writing
    $ifp = fopen( $output_file, 'wb' );

    // split the string on commas
    // $data[ 0 ] == "data:image/png;base64"
    // $data[ 1 ] == <actual base64 string>
    $data = explode( ',', $base64_string );

    // we could add validation here with ensuring count( $data ) > 1
    fwrite( $ifp, base64_decode( $data[ 1 ] ) );

    // clean up the file resource
    fclose( $ifp );

    return $output_file;
}

function save_img(){
    $basedir =  wp_upload_dir();
    $img = $_POST['data'];
    $quiz_id =  $_POST['quiz_id'];
    $val=base64_to_jpeg($img,$basedir['path'].'/'.date('his').'.png');
    $wp_filetype = wp_check_filetype(date('his').'.png', null );
    $attachment = array(
        'post_mime_type' => $wp_filetype,
        'post_title' => preg_replace('/\.[^.]+$/', '',date('his').'.png'),
        'post_content' => '',
        'post_status' => 'inherit'
    );
    $attachment_id = wp_insert_attachment( $attachment,$val );

    $user_id = get_current_user_id();

    $args = array(
        'numberposts'   => -1,
        'post_type'     => 'quizresult',
        'meta_query'    => array(
            'relation'      => 'AND',
            array(
                'key'       => 'quiz_id',
                'compare'   => '=',
                'value'     => $quiz_id,
            ),
            array(
                'key'       => 'user',
                'compare'   => '=',
                'value'     => $user_id,
            )
        )
    );
    $result_id = get_posts($args);
    update_post_meta($result_id, 'student_img', $attachment_id);

}


add_action("wp_ajax_video_save", "video_save");
add_action("wp_ajax_nopriv_video_save", "video_save");

function video_save(){

    $fileName = preg_replace('/\s+/', '-', $_FILES["file"]["name"]);
    // removing special character but keep . character because . seprate to extantion of file
    $fileName = preg_replace('/[^A-Za-z0-9.\-]/', '', $fileName);

    // rename file using time
    $fileName = time().'-'.$fileName;
    $upload_file = wp_upload_bits($fileName, null, file_get_contents($_FILES["file"]["tmp_name"]) );

    if(!$upload_file['error']){
        $wp_filetype = wp_check_filetype($fileName, null);
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', $fileName),
            'post_content' => '',
            'post_status' => 'inherit'
        );
        $attachment_id = wp_insert_attachment($attachment, $upload_file['file']);
        echo json_encode([
            "success" => true,
            "attachment_id"	=> $attachment_id,
        ]);
    }
 die;
}