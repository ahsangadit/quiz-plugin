<?php
add_shortcode('vgc_register_student', 'wpstudent_register');
function wpstudent_register()
{
    $msg = '';
    if (isset($_REQUEST['submit_vgc_student_reg_page'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $email = $_POST['email'];
        if (email_exists($email) == false && username_exists($username) == null) {

            // Create the new user
            $user_id = wp_create_user($username, $password, $email);

            // Get current user object
            $user = get_user_by('id', $user_id);
            // Remove role
            $user->remove_role('subscriber');
            // Add role
            $user->add_role('student');
            update_user_meta($user_id, 'cp_date', $_POST['cp_date']);
            update_user_meta($user_id, 'cp_subject', $_POST['cp_subject']);
            update_user_meta($user_id, 'cp_location', $_POST['cp_location']);
            update_user_meta($user_id, 'cp_grade', '');
            update_user_meta($user_id, 'cp_student_number', 'vgc-' . $user_id);
            update_user_meta($user_id, 'agent_id', $_POST['agent_id']);
            $msg = '<p style="color : green">User Registered Successfully!</p>';
        } else {


            $msg = '<p style="color : red">username/email already exist!</p>';


        }


    }
    $vgc_student_reg_page_id = get_option("vgc_student_reg_page_id");
    $get_agents = get_users( ['role' => 'agent']);

    if(!is_user_logged_in()) {
    $html = '<div class="plugin_form">
    <h2 class="studnet_name">Register New Student</h2>
        <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
          <div class="form_main_div">
            <label><input type="text" name="username" class="plugin_form_input"  placeholder="Enter UserName"   required><i class="fa fa-user" aria-hidden="true"></i>
</label>
            <label><input type="email" name="email" class="plugin_form_input"  placeholder="Enter Email"   required><i class="fa fa-envelope-o" aria-hidden="true"></i>
</label>
            <label><input type="password" name="password" class="plugin_form_input"  placeholder="Enter Password"   required><i class="fa fa-key" aria-hidden="true"></i>
</label>
            <label><input type="date" name="cp_date" class="plugin_form_input"  placeholder="Enter Date"   required><i class="fa fa-calendar" aria-hidden="true"></i>
</label>
               <label><select name="agent_id" class="plugin_form_input"  id="" required>
                <option value="">Select Agent</option>';

              foreach ($get_agents as $a) {

                  $html .= '<option value="' . $a->ID . '">' . $a->user_nicename . '</option>';

              }

               $html .='</select></label>
            <label><select name="cp_subject" class="plugin_form_input"  id="" required>
            <option value="">Select Program</option>';


    $programs = array(
        'post_type' => 'programs',
        'posts_per_page' => -1
    );
    $query_programs = new WP_Query($programs);
    if ($query_programs->have_posts()) :
        while ($query_programs->have_posts()) : $query_programs->the_post();
            $html .= sprintf('<option value="' . get_the_ID() . '">' . get_the_title() . '</option>');
        endwhile;
    endif;

    $html .= '</select></label>
           <label> <input type="text" name="cp_location" class="plugin_form_input"  placeholder="Enter Location"   required><i class="fa fa-map-marker" aria-hidden="true"></i>
</label>
           
        
          </div>
          <button type="submit" name="submit_vgc_student_reg_page" class="submitbtn">SUBMIT</button>
          ' . $msg . '
        </form>
      </div>';

}

else{

    $html = '<div class="alert alert-info">
    User Already Loggedin!
</div>';


}

    return $html;
}