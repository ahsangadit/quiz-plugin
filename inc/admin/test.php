<?php


function my_custom_post_test() {

//labels array added inside the function and precedes args array

    $labels = array(
        'name' => _x( 'tests', 'post type general name' ),
        'singular_name' => _x( 'Quiz', 'post type singular name' ),
        'add_new' => _x( 'Add New', 'Vgc Quiz' ),
        'add_new_item' => __( 'Add New Quiz' ),
        'edit_item' => __( 'Edit Quiz' ),
        'new_item' => __( 'New Quiz' ),
        'all_items' => __( 'All Quiz' ),
        'view_item' => __( 'View Quiz' ),
        'search_items' => __( 'Search Quiz' ),
        'not_found' => __( 'No Quiz found' ),
        'not_found_in_trash' => __( 'No Quiz found in the Trash' ),
        'parent_item_colon' => '',
        'menu_name' => 'VGC Quiz'
    );

// args array

    $args = array(
        'labels' => $labels,
        'description' => 'Create,update,delete of tests',
        'public' => true,
        'menu_position' => 4,
        'supports' => array( 'title' ),
        'has_archive' => true,
    );


    
    $result_labels = array(
        'name' => _x( 'Quiz Results', 'post type general name' ),
        'singular_name' => _x( 'Quiz Result', 'post type singular name' ),
        'add_new' => _x( 'Add New', 'Quiz Result' ),
        'add_new_item' => __( 'Add New Result' ),
        'edit_item' => __( 'Edit Result' ),
        'new_item' => __( 'New Result' ),
        'all_items' => __( 'All Result' ),
        'view_item' => __( 'View Result' ),
        'search_items' => __( 'Search Result' ),
        'not_found' => __( 'No Result found' ),
        'not_found_in_trash' => __( 'No Result found in the Trash' ),
        'parent_item_colon' => '',
        'menu_name' => 'VGC Quiz Result'
    );

// args array

    $result_args = array(
        'labels' => $result_labels,
        'description' => 'Create,update,delete of results',
        'public' => true,
        'menu_position' => 4,
        'supports' => array( 'title'),
        'has_archive' => true,
    );

    $result_labels1 = array(
        'name' => _x( 'Question Category', 'post type general name' ),
        'singular_name' => _x( 'Question Category', 'post type singular name' ),
        'add_new' => _x( 'Add New', 'Question Category' ),
        'add_new_item' => __( 'Add New Question Category' ),
        'edit_item' => __( 'Edit Question Category' ),
        'new_item' => __( 'New Question Category' ),
        'all_items' => __( 'All Question Category' ),
        'view_item' => __( 'View Question Category' ),
        'search_items' => __( 'Search Question Category' ),
        'not_found' => __( 'No Result found' ),
        'not_found_in_trash' => __( 'No Result found in the Trash' ),
        'parent_item_colon' => '',
        'menu_name' => 'Question Category'
    );

// args array

    $result_args1 = array(
        'labels' => $result_labels1,
        'description' => 'Create,update,delete of results',
        'public' => true,
        'menu_position' => 4,
        'supports' => array( 'title'),
        'has_archive' => true,
    );

    register_post_type( 'tests', $args );
    register_post_type( 'quizresult', $result_args );
    register_post_type( 'question_category', $result_args1 );
}


add_action( 'init', 'my_custom_post_test' );

require  'Vgc_Test_Meta_Box.php';
