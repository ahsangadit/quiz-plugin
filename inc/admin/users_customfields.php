<?php
function custom_user_profile_fields($user){
    $get_agents = get_users( ['role' => 'agent']);
    if(is_object($user)) {
        $cp_date = esc_attr(get_the_author_meta('cp_date', $user->ID));
        $cp_subject = esc_attr(get_the_author_meta('cp_subject', $user->ID));
        $cp_location = esc_attr(get_the_author_meta('cp_location', $user->ID));
        $cp_grade = esc_attr(get_the_author_meta('cp_grade', $user->ID));
        $cp_student_number = esc_attr(get_the_author_meta('cp_student_number', $user->ID));
        $agent_id = esc_attr(get_the_author_meta('agent_id', $user->ID));

    }
    else {
        $cp_date = null;
        $cp_subject = null;
        $cp_location = null;
        $cp_grade = null;
        $cp_student_number =  null;
        $agent_id = esc_attr(get_the_author_meta('agent_id', $user->ID));

    }
    ?>
    <h3> profile information</h3>
    <table class="form-table">
        <tr>
            <th><label for="cp_date">Date</label></th>
            <td>
                <input type="date" class="regular-text" name="cp_date" value="<?php echo $cp_date; ?>" id="cp_date" /><br />

            </td>
        </tr>
        <tr>
            <th><label for="cp_subject">Subject</label></th>
            <td>
                <input type="text" class="regular-text" name="cp_subject" value="<?php echo $cp_subject; ?>" id="cp_subject" /><br />

            </td>
        </tr>
        <tr>
            <th><label for="cp_location">Location</label></th>
            <td>
                <input type="text" class="regular-text" name="cp_location" value="<?php echo $cp_location; ?>" id="cp_location" /><br />

            </td>
        </tr>
        <tr>
            <th><label for="cp_grade">Grade</label></th>
            <td>
                <input type="text" class="regular-text" name="cp_grade" value="<?php echo $cp_grade; ?>" id="cp_grade" /><br />

            </td>
        </tr>
        <tr>
            <th><label for="cp_student_number">Student Number</label></th>
            <td>
                <input type="text" class="regular-text" name="cp_student_number" value="<?php echo $cp_student_number; ?>" id="cp_student_number" /><br />

            </td>
        </tr>
        <tr>
            <th><label for="cp_student_number">Select Agent</label></th>
            <td>
                <select name="agent_id" class="plugin_form_input"  id="" required>
                    <option value="">Select Agent</option>';
                    <?php
                     foreach ($get_agents as $a) {
                    ?>


                    <option value="<?php echo $a->ID ?>" <?php if($agent_id == $a->ID){  echo 'selected';  } ?>><?php echo $a->user_nicename ?></option>'

                    <?php

                         }
                        ?>
                 </select>
            </td>
        </tr>



    </table>
    <?php
}
add_action( 'show_user_profile', 'custom_user_profile_fields' );
add_action( 'edit_user_profile', 'custom_user_profile_fields' );
add_action( "user_new_form", "custom_user_profile_fields" );

function save_custom_user_profile_fields($user_id){
    # again do this only if you can
    if(!current_user_can('manage_options'))
        return false;

    # save my custom field
    update_user_meta($user_id, 'cp_date', $_POST['cp_date']);
    update_user_meta($user_id, 'cp_subject', $_POST['cp_subject']);
    update_user_meta($user_id, 'cp_location', $_POST['cp_location']);
    update_user_meta($user_id, 'cp_grade', $_POST['cp_grade']);
    update_user_meta($user_id, 'cp_student_number', $_POST['cp_student_number']);
    update_user_meta($user_id, 'agent_id', $_POST['agent_id']);

}
add_action('user_register', 'save_custom_user_profile_fields');
add_action('profile_update', 'save_custom_user_profile_fields');