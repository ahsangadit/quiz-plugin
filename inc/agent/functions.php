<?php
add_filter( 'page_template', 'vgc_agent_create_template' );
function vgc_agent_create_template( $page_template )
{
    if ( is_page( 'agent-create-test' ) ) {
        $page_template =  plugin_dir_path( __DIR__ ). '/agent/test_create.php';
    }
    if ( is_page( 'user-update-profile' ) ) {
        $page_template =  plugin_dir_path( __DIR__ ). '/agent/update_profile.php';
    }
    if ( is_page( 'quiz-invitation' ) ) {
        $page_template =  plugin_dir_path( __DIR__ ). '/agent/quiz_invitation.php';
    }
    return $page_template;
}
add_action("wp_ajax_vgc_agenttest_dlt", "vgc_agenttest_dlt");
add_action("wp_ajax_nopriv_vgc_agenttest_dlt", "vgc_agenttest_dlt");
function vgc_agenttest_dlt(){

    wp_trash_post($_POST['id']);
    echo 'success';



}
function checkuseraccountpage(){
     $user = wp_get_current_user();
    if($user->roles[0] == 'agent') {

        $useraccounturl =  get_site_url().'/agent-create-test/';
        $currentpage = 'agent-create-test';

    }
    else{

        $useraccounturl =  get_site_url().'/user-account/';
        $currentpage = 'user-account';

    }

    ?>

    <script>
         jQuery('span:contains("User Account")').parent().parent().hide();
        var currentpagevgc = "<?php echo $currentpage; ?>";
        var useraccounturl = "<?php echo $useraccounturl ?>";

        jQuery('span:contains("User Account")').parent().attr("href",useraccounturl);
        if(window.location.href == useraccounturl) {
            jQuery('span:contains("User Account")').parent().parent().addClass("current-menu-item");

        }
        <?php
        if ( is_user_logged_in() ) {
        ?>

        jQuery('span:contains("login")').parent().parent().hide();
        jQuery('span:contains("Student Registration")').parent().parent().hide();
        jQuery('span:contains("User Account")').parent().parent().show();

        <?php } ?>




    </script>

<?php
}
add_action('wp_footer','checkuseraccountpage');
?>
