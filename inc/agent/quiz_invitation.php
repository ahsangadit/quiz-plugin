<?php

get_header();
$user_id = '';
if(isset($_REQUEST['user_id'])) {
    $user_id = $_GET['user_id'];
}
if(is_user_logged_in()){

    $user = get_user_by('id',$user_id);

}
$posts_test = get_posts([
    'post_type' => 'tests',
    'post_status' => 'publish',
    'numberposts' => -1
    // 'order'    => 'ASC'
]);
if(isset($_POST['invitation_quiz_submit'])) {
    $test_id = $_POST['select_test'];
    $start_date = $_POST['start_date'];
    $end_date = $_POST['end_date'];
    $description = $_POST['description'];
    $get_assign_students = get_post_meta($test_id, 'assign_students', true);
    $get_assign_students = explode(',', $get_assign_students);
    if (in_array($user_id, $get_assign_students)) {


        $msg = '<div class="form_section_alert">
           <div class="alert-box-danger" role="alert">
              Quiz Already Assigned!
           </div>

       </div>';

    }
    else {
        //php mailer variables
        $message = ' <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>A Simple Responsive HTML Email</title>
  <style type="text/css">
  body {margin: 0; padding: 0; min-width: 100%!important;}
  img {height: auto;}
  .content {width: 100%; max-width: 600px;}
  .header {padding: 40px 30px 20px 30px;}
  .innerpadding {padding: 30px 30px 30px 30px;}
  .borderbottom {border-bottom: 1px solid #f2eeed;}
  .subhead {font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;}
  .h1, .h2, .bodycopy {color: #153643; font-family: sans-serif;}
  .h1 {font-size: 33px; line-height: 38px; font-weight: bold;}
  .h2 {padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;}
  .bodycopy {font-size: 16px; line-height: 22px;}
  .button {text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;}
  .button a {color: #ffffff; text-decoration: none;}
  .footer {padding: 20px 30px 15px 30px;}
  .footercopy {font-family: sans-serif; font-size: 14px; color: #ffffff;}
  .footercopy a {color: #ffffff; text-decoration: underline;}
  @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
  body[yahoo] .hide {display: none!important;}
  body[yahoo] .buttonwrapper {background-color: transparent!important;}
  body[yahoo] .button {padding: 0px!important;}
  body[yahoo] .button a {background-color: #effb41; padding: 15px 15px 13px!important;}
  body[yahoo] .unsubscribe {display: block; margin-top: 20px; padding: 10px 50px; background: #2f3942; border-radius: 5px; text-decoration: none!important; font-weight: bold;}
  }
  /*@media only screen and (min-device-width: 601px) {
  .content {width: 600px !important;}
  .col425 {width: 425px!important;}
  .col380 {width: 380px!important;}
  }*/
  </style>
  </head>
  <body yahoo bgcolor="#ffffff">
  <table width="100%" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <!--[if (gte mso 9)|(IE)]>
        <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
        <td>
        <![endif]-->
      <table bgcolor="#ffffff" class="content" align="center" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td bgcolor="#812881" class="header">
          <table width="70" align="left" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="70" style="padding: 0 20px 20px 0;">
              <img class="fix" src=https://webdesigncompanyinvancouver.com/members/vancouvergenc/wp-content/uploads/2022/03/VGC-International-College-Logo_Purple_rgb-1.png" width="70" height="70" border="0" alt=""/>
            </td>
          </tr>
          </table>
          <!--[if (gte mso 9)|(IE)]>
            <table width="425" align="left" cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td>
            <![endif]-->
          <table class="col425" align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 425px;">
          <tr>
            <td height="70">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="subhead" style="padding: 0 0 0 3px;">
                   Quiz
                </td>
              </tr>
              <tr>
                <td class="h1" style="padding: 5px 0 0 0;">
                   Invitation
                </td>
              </tr>
              </table>
            </td>
          </tr>
          </table>
          <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
      </tr>
      <tr>
        <td class="innerpadding borderbottom">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="h2">
              Dear , ' . $user->user_nicename . '
            </td>
          </tr>
          <tr>
            <td class="bodycopy">
               ' . $_POST['description'] . '
            </td>
          </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td class="innerpadding borderbottom">
          <table width="115" align="left" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="115" style="padding: 0 20px 20px 0;">
              <img class="fix" src="http://placehold.it/115x115" width="115" height="115" border="0" alt=""/>
            </td>
          </tr>
          </table>
          <!--[if (gte mso 9)|(IE)]>
            <table width="380" align="left" cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td>
            <![endif]-->
         <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
      </tr>
      <tr>
        <td class="innerpadding borderbottom">
          <img class="fix" src="http://placehold.it/600x200" width="100%" border="0" alt=""/>
        </td>
      </tr>
    
      <tr>
        <td class="footer" bgcolor="#812881">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" class="footercopy">
               &reg; All Right Reserved, ' . date('Y') . '<br/>
 
            
            </td>
          </tr>
          <tr>
            <td align="center" style="padding: 20px 0 0 0;">
             
            </td>
          </tr>
          </table>
        </td>
      </tr>
      </table>
      <!--[if (gte mso 9)|(IE)]>
  </td>
  </tr>
  </table>
  <![endif]-->
    </td>
  </tr>
  </table>
  </body>';

        $to = $user->user_email;
        $subject = "Test Invitation";
        $headers[] = 'From: VGC <' . get_option('admin_email') . '>';
        $content_type = function () {
            return 'text/html';
        };
        add_filter('wp_mail_content_type', $content_type);
        $sent = wp_mail($to, $subject, $message, $headers);
        if ($sent) {
            array_push($get_assign_students, $user_id);
            $students = implode(",", $get_assign_students);
            update_post_meta(
                $test_id,
                'assign_students',
                $students
            );
            $msg = '<div class="form_section_alert">
           <div class="alert-box-success" role="alert">
            Invitation Successfully Sent!
           </div>

       </div>';

        }
        else {
            $msg = '<div class="form_section_alert">
           <div class="alert-box-danger" role="alert">
          Something Went Wrong!
           </div></div>';


        }


    }


}




?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
<style>
    .container_section_form {
        max-width: 760px;
        margin: auto;
    }


    .container_section_form .row_col {
        display: flex;
        width: 100%;
        justify-content: center;
    }


    .war_col_half {
        width: 50%;
    }

    .war_col_half input {
        width: 95%;
        background-color: rgb(255, 255, 255);
        box-shadow: 0px 6px 20px 0px rgba(0, 0, 0, 0.08);
        height: 50px;
        border: none;
    }

    .war_col_half {
        padding: 10px;
    }

    .container_section_form .row_col input {
        padding: 0 10px;
        display: block;
    }


    .war_col_half select {
        width: 100%;
        background-color: rgb(255, 255, 255);
        box-shadow: 0px 6px 20px 0px rgb(0 0 0 / 8%);
        height: 50px;
        border: none;
        padding: 0 10px;
    }
    :focus{
        outline: none;
    }
    input.btn_text_submit {
        background-color: rgb(129, 39, 129);
        width: 310px;
        height: 50px;
        border: none;
        color: white;
        font-family: 'Source Sans Pro' !important;
    }
    input.btn_text_submit {
        display: block;
        margin: auto;
        margin-top: 30px;
    }
    input.btn_text_submit {
        font-size: 16px !important;
    }
    .war_col_full {
        width: 100%;
    }


    .war_col_full textarea {
        width: 94.5%;
        background-color: rgb(255, 255, 255);
        box-shadow: 0px 6px 20px 0px rgb(0 0 0 / 8%);
        border: none;
        display: block;
        padding: 10px;
    }

    .war_col_full {
        padding: 10px;
    }
    .row_col_box {
        text-align: center;
        margin-top: 35px;
        margin-bottom: 39px !IMPORTANT;
    }
    .container_section_form {
        font-family: 'Source Sans Pro' !important;
    }

    .container_section_form h2 {
        text-align: center;
        font-size: 42px;
        font-weight: 600 !important;
    }
    label {
    }

    .row_col label {
        display: block;
        position: relative;
    }

    .row_col i {
        position: absolute;
        right: 8px;
        top: 17px;
        background: white;
        color: #812781;
    }

    .war_col_full label {
        display: block;
        position: relative;
    }

    .war_col_full i {
        position: absolute;
        right: 28px;
        top: 14px;
        color: #812781;
    }


    .row_col_box label {
        font-family: 'Source Sans Pro' !important;
        color: #303030;
    }

    .row_col_box label a {
        text-decoration: none;
        color: #812781;
    }
    select {
        -webkit-appearance: none;
        -moz-appearance: none;
        text-indent: 1px;
        text-overflow: '';
    }
    i.fa.fa-angle-down {
        font-weight: bold;
    }
    .war_col_full label {
        display: block;
        position: relative;
        width: 100%;
    }

    .war_col_full {
        padding: 10px;
        display: flex;
        width: 100%;
    }
    .container_section_form {
        padding: 64px 0px;
    }

    .container_section_form h2 {
        margin-top: 0;
    }

    input.btn_text_submit {
        cursor: pointer;
    }
    /*input[type="date"]::-webkit-calendar-picker-indicator {*/

    /*    z-index: 9999;*/
    /*    opacity:0*/

    /*}*/
    .container_section_form h2 {
        margin: 0;
    }

    .deatil_warp {
        text-align: center;
        padding: 19px 0;
    }

    .deatil_warp a {
        color: #404040;
        padding: 0 10px;
        text-decoration: none;
    }
    select.select_text {
        border-style: solid;
        border-width: 1px;
        border-color: rgb(144, 144, 144);
        background-color: rgba(176, 176, 176, 0);
        width: 234px;
        height: 34px;
        padding: 0 10px;
        margin: auto;
        display: block;
        margin-top: 20px;
    }

    .deatil_warp h3 {
        margin-top: 0;
        font-size: 24px;
        margin-bottom: 10px;
    }

    .deatil_warp h4 {
        font-size: 24px;
        color: #000000;
        font-weight: 500 !important;
        margin: 0;
        margin-top: 20px;
    }
    label.label_text {
        display: block;
        position: relative;
        margin: auto;
        width: 234px;
    }

    label.label_text i {
        position: absolute;
        top: 6px;
        right: 5px;
        color: #812781;
    }
    .alert-box-success {
        color: #155724;
        background-color: #d4edda;
        border-color: #c3e6cb;
        padding: 7PX;
    }
    .alert-box-danger {
        color: #721c24;
        background-color: #f8d7da;
        border-color: #f8d7da;
        padding: 7PX;
    }
    .form_section_alert {
        padding: 0 10px;
    }
    .row_col label {padding: 0 !important;}

    .war_col_half select {display: block !important;}

    .war_col_full textarea {width: 100% !IMPORTANT;display: block !important;}

    .war_col_full label {padding: 0 !important;}


    .war_col_full {width: 97% !important;}

    .container_section_form .row_col input {width: 100% !important;}

    input.btn_text_submit {box-shadow: unset !important;}

    .row_col_box {display: flex;align-items: center;justify-content: center;}

    .row_col_box input {margin-right: 10px;}

    select.select_text {padding: 0 10px!important;}



    .row_col i {z-index: 99;right: 9px;}
</style>
<div class=container_section_form>
    <h2>Send Quiz Invitations</h2>

    <form action="#" method="post">

        <label class="label_text" for="">
            <select class="select_text" name="select_test" id="" required>
                <option value="">Select Test</option>
                <?php

                foreach($posts_test as $t){

                ?>
                <option value="<?php echo $t->ID;  ?>"><?php echo $t->post_title;  ?></option>
                <?php } ?>
            </select>
            <i class="fa fa-sort-desc" aria-hidden="true"></i>
        </label>
        <div class="deatil_warp">
            <h3><?php  echo $user->user_nicename; ?></h3>
            <a href="#">  <?php echo $user->user_email; ?> </a> / <a href="#">  Canada </a>  /  <a href="#"> Category Type</a>
            <h4>Quiz Detail</h4>
        </div>
        <?php
        if(isset($msg)){

            echo $msg;
        }
        ?>

        <div class="row_col">
            <div class="war_col_half">
                <label>
                    <input type="text" id="start_date" class="date-picker" name="start_date" value="Jan 10, 1995" placeholder="date">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                </label>
            </div>
            <div class="war_col_half">
                <label>
                    <input type="text" id="end_date" class="date-picker" name="end_date" value="Jan 10, 1995" placeholder="date">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                </label>
            </div>

        </div>
<!--        <div class="row_col">-->
<!--            <div class="war_col_half">-->
<!--                <label>-->
<!--                    <select name="" id="">-->
<!--                        <option value="Select Category">Select Category</option>-->
<!--                        <option value="Select Category"> Category 1</option>-->
<!--                        <option value="Select Category"> Category 2</option>-->
<!--                        <option value="Select Category"> Category 3</option>-->
<!--                    </select>-->
<!--                    <i class="fa fa-angle-down" aria-hidden="true"></i>-->
<!--                </label>-->
<!--            </div>-->
<!--            <div class="war_col_half">-->
<!--                <label>-->
<!--                    <select name="" id="">-->
<!--                        <option value="Select Category">Select Category</option>-->
<!--                        <option value="Select Category"> Category 1</option>-->
<!--                        <option value="Select Category"> Category 2</option>-->
<!--                        <option value="Select Category"> Category 3</option>-->
<!--                    </select>-->
<!--                    <i class="fa fa-angle-down" aria-hidden="true"></i>-->
<!--                </label>-->
<!---->
<!--            </div>-->
<!---->
<!--        </div>-->
        <div class="war_col_full">
            <label>
                <textarea name="description" id="" cols="30" rows="10" placeholder="Student Detail"></textarea><i class="fa fa-pencil" aria-hidden="true"></i>
            </label>
        </div>
        <div class="row_col_box">
            <input type="checkbox" required> <label >Agree <a href="#"> Terms & Conditions</a></label>
        </div>

        <input class="btn_text_submit" name="invitation_quiz_submit" type="submit" value="Save">
    </form>
</div>
<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script>
    jQuery(document).ready(function () {
        jQuery('.date-picker').datepicker({
            format: 'dd-mm-yyyy',
            startDate: '+1d'
        });
    });
</script>
<?php
get_footer();
?>
