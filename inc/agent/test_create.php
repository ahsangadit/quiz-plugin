<?php
header("Cache-Control: no-cache, must-revalidate");
clearstatcache();
get_header();

$posts_programs = get_posts([
    'post_type' => 'Programs',
    'post_status' => 'publish',
    'numberposts' => -1
    // 'order'    => 'ASC'
]);

$user = wp_get_current_user();
//$get_test = new WP_Query( "post_type=tests&meta_key=test_total_marks&meta_value='.$user->ID.'&order=ASC" );
$get_test = get_posts(array(
    'posts_per_page'   => -1,
    'post_type'        => 'tests',
    'author'        =>  get_current_user_id(),
));

$assign_students = get_users( ['role' => 'student']);
$studentlist = get_users(array(
    'meta_key' => 'agent_id',
    'meta_value' => $user->ID
));
if(is_user_logged_in()) {
    $allowed_users = ['agent','administrator'];
    // Get the user object.
    $user = get_userdata(get_current_user_id());
    if(in_array($user->roles[0],$allowed_users)){
?>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap" rel="stylesheet">
        <style>
            .container_section {
                max-width: 1170px;
                margin: auto;
            }

            .student_name_row {
                display: flex;
                justify-content: space-between;
                align-items: center;
            }

            .student_name h2 {font-family: 'Source Sans Pro' !important;font-size: 42px;font-weight: 500 !important;}

            .request_btn a {
                background-color: rgb(129, 39, 129);
                width: 193px;
                height: 50px;
                display: flex;
                align-items: center;
                justify-content: center;
                color: white !important;
                text-decoration: none;
                font-family: 'Source Sans Pro' !important;
            }

            .request_btn {}

            .student_information ul {
                padding: 0;
                margin: 0;
                list-style: none;
                display: flex;
            }

            .student_information ul li {
                padding: 0 25px !important;
            }

            .student_name h2 {
                margin: 0;
            }

            .student_information {
                margin-top: 20px;
            }

            p.dec_for_student {
                color: #707070 !important;
                font-family: 'Source Sans Pro' !important;
                line-height: 1.5;
                font-size: 14px;
            }

            .student_information li a {
                font-family: 'Source Sans Pro';
                font-size: 14px;
                color: #404040;
                text-decoration: none;
            }

            ul.quiz_head {
                padding: 0;
                margin: 0;
                list-style: none;
            }

            ul.quiz_body {
                padding: 0;
                list-style: none;
                font-family: 'Source Sans Pro';
            }

            ul.quiz_head {
                font-family: 'Source Sans Pro';
            }
            ul.quiz_body li {
                display: flex;
            }

            .quiz_section ul li span:first-child {
                width: 374px !important;
                display: inline-block;
            }

            .quiz_section ul li span {
                width: 162px;
                display: inline-block;
            }
            .comment_section {
                font-family: 'Source Sans Pro';
            }
            .quiz_head table.table {
                margin: auto;
                max-width: 100%;
                border-collapse: collapse;
                width: 100%;
                text-align: left;
            }



            .quiz_head  table.table tbody td {
                background: #f4f4f4;
                padding: 8px 5px;
                margin-bottom: 10px !important;
                /* display: block; */
                font-size: 14px;
            }

            .quiz_head  table.table tr {
                border-bottom: solid 10px white;
            }

            .quiz_head  table.table tr {
                margin-bottom: 10px !important;
                width: 100%;
            }

            .quiz_head {
                font-family: 'Source Sans Pro';
            }

            .quiz_head  table.table tbody td {
                margin-bottom: 10px;
            }


            .quiz_head tbody tr td:last-child {
                background: white;
                text-align: center;
                padding: 0;
                padding-left: 10px;
            }

            .comment_details {
                padding-bottom: 20px;
                border-bottom: solid 1px #d7d7d7;
                margin: 20px 0;
            }



            .comment_section textarea {
                width: 100%;
            }

            .quiz_head tbody tr td:last-child a {
                background: #812781;
                display: block;
                padding: 9px;
                text-decoration: none;
                color: white;
            }

            .quiz_head tbody tr td:last-child a:hover {
                background: #72c13d;
            }

            table.table tbody td:first-child {
                font-weight: 600 !important;
            }

            .quiz_head th {
                color: #812781;
                font-family: 'Source Sans Pro' !important;
                text-transform: uppercase;
                font-size: 14px;
            }



            .quiz_head table.table {
                margin-top: 25px;
            }
            .quiz_head table.table {
                margin: auto !important;
            }

            .content-area {
                background: white;
            }

            .quiz_head th, .quiz_head td {
                border: navajowhite;
            }

            .quiz_head table.table {
                border: navajowhite;
            }

            .quiz_head th {
                background: white !important;
            }
            div#hb-page-title {
                display: none;
            }
            .student_information i {
                position: absolute;
                left: 0;
                top: 4px;
            }

            .student_information ul li {
                position: relative;
                padding-left: 19px !important;
            }

            .comment_section textarea {
                width: 100%;
                background-color: rgb(255, 255, 255);
                box-shadow: 0px 6px 20px 0px rgb(0 0 0 / 8%);
                border: navajowhite;
                height: 160px;
            }
            .student_information i {
                color: #812781;
                font-weight: bold !important;
            }

            .student_information li a {
                font-weight: 600 !important;
            }
            input.comment_submit_btn {
                background-color: rgb(129, 39, 129);
                width: 120px;
                height: 46px;
                box-shadow: unset !important;
                position: absolute;
                right: 0;
                z-index: 11;
                bottom: 17px;
                right: 20px;
            }



            .comment_section form {
                position: relative;
            }

            .container_section {
                padding-top: 55px;
            }
            h2.quiz_title {
                font-family: 'Source Sans Pro' !important;
                font-size: 42px;
                font-weight: 500 !important;
                margin: 20px 0px;
            }
            .search_agent {
                background-color: rgb(255, 255, 255);
                box-shadow: 0px 6px 20px 0px rgba(0, 0, 0, 0.08);
                width: 370px;
                height: 50px;
                border: none;
            }
            :focus {outline: none;}
            .search_agent {
                padding: 10px !important;
            }
            .search_agent {
                padding: 10px !important;
            }

            .agent_row {
                display: flex;
                justify-content: space-between;
                align-items: center;
            }

            .agent_row h2 {
                text-transform: capitalize;
                font-family: 'Source Sans Pro';
                font-weight: 500 !important;
                font-size: 42px;
            }
            .details input {
                border-style: solid !important;
                border-width: 1px;
                border-color: rgb(144, 144, 144) !important;
                width: 134px;
                height: 34px;
            }

            .details select {
                border-style: solid !important;
                border-width: 1px;
                border-color: rgb(144, 144, 144) !important;
                width: 134px;
                height: 34px;
            }
            input.search_btn {
                position: absolute;
                right: 0;
                z-index: 1;
                background: transparent;
                height: 100%;
                border: none !important;
                opacity: 0;
                cursor: pointer;
            }

            form.search_form_agent {
                position: relative;
            }


            form.search_form_agent i.fa.fa-search {
                right: 10px;
                position: absolute;
                top: 17px;
            }
            td.edit_quiz {
                text-align: center;
                background: #fff0ff !important;
            }
            tr.active td {
                background: #fff0ff !important;
            }

            tr.active a.btn_quiz {
                background: #4b004f !important;
            }

            tr.active td.edit_quiz {
                background: #812781 !important;
                color: white;
            }

            td.edit_quiz {
                color: #812781;
            }
            .request_btn span {
                width: 50px;
                text-align: center;
                display: inline-block;
                height: 50px;
                line-height: 50px;
                background: #4b004f;
                margin-right: 20px;
            }

            .request_btn a {
                display: inline-block;
                padding-right: 12px;
            }
            form.search_form_agent i.fa.fa-search {
                color: #812781;
            }
            .details select {
                text-transform: capitalize;
            }
        </style>
        <div class="container_section">
            <div class="student_name_row">
                <div class="student_name">

                    <form class="search_form_agent" action="/action_page.php">

                        <input class="search_agent" type="search" id="gsearch" name="gsearch" placeholder="Search">
                        <input class="search_btn" type="submit"
                        >
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </form>
                </div>
                <div class="request_btn">
                    <a href="#"> <span><i class="fa fa-plus" aria-hidden="true"></i>
               </span> Send Quiz Invite</a>
                </div>

            </div>

            <div class="agent_row">
                <div class="title">
                    <h2>Student List</h2>
                </div>
                <div class="details">
                    <input type="date" value="2022-01-01" min="2005-01-01" max="2012-01-01">
                    <select class="country">
                        <option>country</option>
                        <option value="usa">United States</option>
                        <option value="uk">United Kingdom</option>
                    </select>
                    <select class="Category">
                        <option>Category</option>
                        <option value="usa">Category 1</option>
                        <option value="usa">Category 2</option>
                        <option value="usa">Category 3</option>
                    </select>
                </div>


            </div>

            <div class="quiz_section">
                <div class="quiz_head">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Student</th>
                            <th>Category</th>
                            <th>Register On</th>
                            <th>Location</th>
                            <th>status</th>
                            <th>grade</th>
                            <th>number</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($studentlist as $student){
                            $cp_location = esc_attr(get_the_author_meta('cp_location', $student->ID));
                            $cp_grade = esc_attr(get_the_author_meta('cp_grade', $student->ID));
                            $cp_subject = esc_attr(get_the_author_meta('cp_subject', $student->ID));
                            $get_programs = get_posts(array(
                                    'post_type'        => 'programs',

                            ));



                            ?>
                        <tr>
                            <td data-label="Contact ID"><?php echo $student->user_nicename ?></td>

                            <td data-label="Power"><?php
                            foreach($get_programs as $p) {
                                if ($p->ID == $cp_subject) {

                                    echo $p->post_title;


                                }
                            }
                             ?></td>
                            <td data-label="Power"><?php echo  $student->user_registered ?></td>
                            <td data-label="Power"><?php echo  $cp_location; ?></td>
                            <td data-label="Power">Status Type</td>
                            <td data-label="Power">Grade Type</td>
                            <td data-label="Power">123-456-7890</td>
                            <td class="edit_quiz" data-label="Power">
                                <a href="<?= home_url() ?>/user-update-profile?user_id=<?= $student->ID; ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            </td>
                            <td data-label="Power"><a class="btn_quiz" href="<?= home_url() ?>/quiz-invitation?user_id=<?= $student->ID; ?>">Send Quiz</a></td>
                        </tr>


                            <?php } ?>


                        </tbody>
                    </table>

                </div>
            </div>




        </div>
<?php

    }
    else{

        global $wp_query;
        $wp_query->set_404();
        status_header( 404 );
        get_template_part( 404 ); exit();
    }
}
else{

    global $wp_query;
    $wp_query->set_404();
    status_header( 404 );
    get_template_part( 404 ); exit();


}
get_footer();


?>