<?php

get_header();
$user_id = '';
if(isset($_REQUEST['user_id'])) {
    $user_id = $_GET['user_id'];
}
if(is_user_logged_in()){

    $user = get_user_by('id',$user_id);

}


?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap" rel="stylesheet">

<style>
    .container_section_form {
        max-width: 760px;
        margin: auto;
    }


    .container_section_form .row_col {
        display: flex;
        width: 100%;
        justify-content: center;
    }


    .war_col_half {
        width: 50%;
    }

    .war_col_half input {
        width: 96%;
        background-color: rgb(255, 255, 255);
        box-shadow: 0px 6px 20px 0px rgba(0, 0, 0, 0.08);
        height: 50px;
        border: none;
    }

    .war_col_half {
        padding: 10px;
    }

    .container_section_form .row_col input {
        padding: 0 10px;
        display: block;
    }


    .war_col_half select {
        width: 100%;
        background-color: rgb(255, 255, 255);
        box-shadow: 0px 6px 20px 0px rgb(0 0 0 / 8%);
        height: 50px;
        border: none;
        padding: 0 10px;
    }
    :focus{
        outline: none;
    }
    input.btn_text_submit {
        background-color: rgb(129, 39, 129);
        width: 310px;
        height: 50px;
        border: none;
        color: white;
        font-family: 'Source Sans Pro' !important;
    }
    input.btn_text_submit {
        display: block;
        margin: auto;
        margin-top: 30px;
    }
    input.btn_text_submit {
        font-size: 16px !important;
    }
    .war_col_full {
        width: 100%;
    }


    .war_col_full textarea {
        width: 100%;
        background-color: rgb(255, 255, 255);
        box-shadow: 0px 6px 20px 0px rgb(0 0 0 / 8%);
        border: none;
        margin: auto !IMPORTANT;
        display: block;
        padding: 10px;
    }

    .war_col_full {
        padding: 10px;
    }
    .row_col_box {
        text-align: center;
        margin-top: 35px;
        margin-bottom: 39px !IMPORTANT;
    }
    .container_section_form {
        font-family: 'Source Sans Pro' !important;
    }

    .container_section_form h2 {
        text-align: center;
        font-size: 42px;
        font-weight: 600 !important;
    }
    label {
    }

    .row_col label {
        display: block;
        position: relative;
    }

    .row_col i {
        position: absolute;
        right: 8px;
        top: 17px;
        background: white;
        color: #812781;
    }

    .war_col_full label {
        display: block;
        position: relative;
    }

    .war_col_full i {
        position: absolute;
        right: 20px;
        top: 14px;
        color: #812781;
    }


    .row_col_box label {
        font-family: 'Source Sans Pro' !important;
        color: #303030;
    }

    .row_col_box label a {
        text-decoration: none;
        color: #812781;
    }
    select {
        -webkit-appearance: none;
        -moz-appearance: none;
        text-indent: 1px;
        text-overflow: '';
    }
    i.fa.fa-angle-down {
        font-weight: bold;
    }
    .war_col_full label {
        display: block;
        position: relative;
        width: 95.5%;
    }

    .war_col_full {
        padding: 10px;
        display: flex;
        width: 100%;
    }
    .container_section_form {
        padding: 64px 0px;
    }

    .container_section_form h2 {
        margin-top: 0;
    }

    input.btn_text_submit {
        cursor: pointer;
    }
</style>
<div class=container_section_form>
    <h2>Update John Jone's Profile</h2>
    <form action="/action_page.php">

        <div class="row_col">
            <div class="war_col_half">
                <label>
                    <input type="text" id="fname" name="fname" value="John" placeholder="First Name">
                    <i class="fa fa-user-o" aria-hidden="true"></i>

                </label>
            </div>
            <div class="war_col_half">
                <label>
                    <input type="text" id="lname" name="lname" value="Doe" placeholder="Last Name"></label>
            </div>

        </div>
        <div class="row_col">
            <div class="war_col_half">
                <label>
                    <input type="email" id="fname" name="fname" value="test@testemail.com" placeholder="Email">
                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                </label>
            </div>
            <div class="war_col_half">
                <label>
                    <input type="phone" id="lname" name="lname" value="123-456-7890" placeholder="Phone"><i class="fa fa-phone" aria-hidden="true"></i>
                </label>
            </div>

        </div>
        <div class="row_col">
            <div class="war_col_half">
                <label>
                    <input type="date" id="fname" name="fname" value="Jan 10, 1995" placeholder="date">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                </label>
            </div>
            <div class="war_col_half">
                <label>
                    <select name="" id="">
                        <option value="Select Category">Select Category</option>
                        <option value="Select Category"> Category 1</option>
                        <option value="Select Category"> Category 2</option>
                        <option value="Select Category"> Category 3</option>
                    </select>
                    <i class="fa fa-angle-down" aria-hidden="true"></i>

            </div>

        </div>
        <div class="row_col">
            <div class="war_col_half">
                <label>
                    <input type="text" id="fname" name="fname" value="01234 Lorem ipsum, City" placeholder="Location"><i class="fa fa-map-marker" aria-hidden="true"></i>
                </label>
            </div>
            <div class="war_col_half">
                <label>
                    <input type="text" id="lname" name="lname" value="Canada" placeholder="city"><i class="fa fa-map" aria-hidden="true"></i>
                </label>
            </div>

        </div>
        <div class="row_col">
            <div class="war_col_half">
                <label>
                    <input type="text" id="fname" name="fname" value="" placeholder="Enter New Password"><i class="fa fa-eye" aria-hidden="true"></i>
                </label>
            </div>


        </div>

        <div class="war_col_full">
            <label>
                <textarea name="" id="" cols="30" rows="10" placeholder="Student Detail"></textarea><i class="fa fa-pencil" aria-hidden="true"></i>
            </label>
        </div>




        <div class="row_col_box">
            <input type="checkbox"> <label >Agree <a href="#"> Terms & Conditions</a></label>
        </div>

        <input class="btn_text_submit" type="submit" value="Save">
    </form>
</div>

<?php
get_footer();
?>
