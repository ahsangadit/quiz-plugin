<?php

get_header();
if(isset($_GET['quiz'])){
    $quiz_id = $_GET['quiz'];
}
$user_id = get_current_user_id();
$args = array(
    'numberposts'   => -1,
    'post_type'     => 'quizresult',
    'meta_query'    => array(
        'relation'      => 'AND',
        array(
            'key'       => 'quiz_id',
            'compare'   => '=',
            'value'     => $quiz_id,
        ),
        array(
            'key'       => 'user',
            'compare'   => '=',
            'value'     => $user_id,
        )
    )
);
$result_id = get_posts($args);
$count_q = get_post_meta($result_id[0]->ID , 'question_count' , true );

$questions_in = get_post_meta( $result_id[0]->ID, 'question_ids', true );
if($questions_in == ''){
    $questions_in = [];
}
$questions_in = [];
// var_dump($questions_in);
$get_time = get_post_meta($result_id[0]->ID , 'time', true);

$complete_time = get_post_meta($result_id[0]->ID , 'complete_time', true);

if($complete_time){ ?>
    <script>
        window.location.href= "<?= home_url() ?>/user-account"
    </script>

    <?php
}

$questions = [];
$args = array(
    'post_type'     => 'questions',
    'meta_query' => array(
        array(
            'key' => 'questions_type',
            'value' => 'grammer'
        ),
    ),
    'orderby' => 'rand',
    'posts_per_page' => -1,
);

$questions = get_posts( $args );
$questions_get = [];

if($user_id != 0){
    foreach($questions as $key => $val){
        if( (!in_array( $val->ID, $questions_in) ) ){
            $questions_get[$key]['question_id'] = $val->ID;
            $questions_get[$key]['question'] = $val->post_title;
            $questions_get[$key]['question_timelimit'] = get_post_meta( $val->ID, 'question_timelimit', true );
            $questions_get[$key]['program_id'] = get_post_meta( $val->ID, 'questions_programid', true );
        }
    }
}

$test_timelimit = get_post_meta( $quiz_id, 'test_timelimit', true );
$test_passinglevel = get_post_meta( $quiz_id, 'test_passinglevel', true );
$test_programid = get_post_meta( $quiz_id, 'test_programid', true );
$test_total_marks = get_post_meta( $quiz_id, 'test_total_marks', true );
$total_q = count($questions);

$total_q_get =  count($questions_in);
if($questions_in != ''){
    $total_count = $total_q - $total_q_get;
}
   
?>
  <script>
        const questions =  <?= json_encode($questions_get); ?>;
        const quiz_id = <?= $quiz_id ?>;
        const user_id = <?= $user_id ?>;
        const no_q =  <?= ($questions_in != '') ? $total_count : $total_q ?>;
        const test_time =  "<?= $get_time == '' ? $test_timelimit.":00" : $get_time ?>";
        const program_id =  <?= $test_programid ?>;
        const passing_range =  <?= $test_passinglevel ?>;
        //    var timer2 = test_time+":01";
        var timer2 = test_time;
        var interval = setInterval(function() {
            var timer = timer2.split(':');
            //by parsing integer, I avoid all extra string processing
            var minutes = parseInt(timer[0], 10);
            var seconds = parseInt(timer[1], 10);
            --seconds;
            minutes = (seconds < 0) ? --minutes : minutes;
            if (minutes < 0) clearInterval(interval);
            seconds = (seconds < 0) ? 59 : seconds;
            seconds = (seconds < 10) ? '0' + seconds : seconds;
            jQuery('.countdown').html(minutes + ':' + seconds);
            timer2 = minutes + ':' + seconds;
        }, 1000);

        function get_time(){
            jQuery.ajax({
                type: "POST",
                url:  '<?= admin_url('admin-ajax.php'); ?>',
                data : {
                    action: "get_time_data",
                    quiz_id : quiz_id,
                    time: timer2
                },
            }).success(function(response){
                var data = JSON.parse(response);
                console.log(data.time);
            });
        }

        setInterval(get_time, 60000);



        let shuffledQuestions = [] //empty array to hold shuffled selected questions

        function handleQuestions() {
            //function to shuffle and push 10 questions to shuffledQuestions array
            while ( shuffledQuestions.length <= no_q-1 ) {
                const random = questions[Math.floor(Math.random() * questions.length)]
                if (!shuffledQuestions.includes(random)) {
                    shuffledQuestions.push(random)
                }
            }
        }

        let questionNumber = 1
        let playerScore = 0
        let wrongAttempt = 0
        let indexNumber = 0

        // function for displaying next question in the array to dom
        function NextQuestion(index) {
            handleQuestions()
            const currentQuestion = shuffledQuestions[index];
            document.getElementById("question-number").innerHTML = questionNumber
             jQuery(".q_no").html(questionNumber)
            document.getElementById("display-question").innerHTML = currentQuestion.question;

            jQuery('.question_next').attr('data-question_no',questionNumber);
            jQuery('.question_next').attr('data-question',currentQuestion.question_id);


            if(currentQuestion.question_timelimit != ''){
                jQuery('.limit-q').css('display','block');
                var timer2 = currentQuestion.question_timelimit+":01";
                var interval = setInterval(function() {
                    var timer = timer2.split(':');
                    //by parsing integer, I avoid all extra string processing
                    var minutes = parseInt(timer[0], 10);
                    var seconds = parseInt(timer[1], 10);
                    --seconds;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    if (minutes < 0) clearInterval(interval);
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    jQuery('.limit').html(minutes + ':' + seconds);
                    timer2 = minutes + ':' + seconds;
                    if(minutes == '0' && seconds == '00'){
                        handleNextQuestion();
                    }
                }, 1000);
            }else{
                jQuery('.limit-q').css('display','none');
            }


        }


        function checkForAnswer() {
            console.log(indexNumber);
            const currentQuestion = shuffledQuestions[indexNumber] //gets current Question
            var get_ans = jQuery('.get_ans').val(); //gets current Question's answer
            
            const options = document.getElementsByName("option"); //gets all elements in dom with name of 'option' (in this the radio inputs)
            let correctOption = null
              jQuery.ajax({
                        type : "post",
                        url : '<?= admin_url('admin-ajax.php'); ?>',
                        data : {
                            action: "save_result_res",
                            quiz_id : quiz_id,
                            type:'grammer',
                            answer : get_ans,
                            question : currentQuestion.question,
                            question_id : currentQuestion.question_id,
                            correct_ans : '',
                            time : timer2,
                            question_no: questionNumber-1,
                            is_quiz: 'grammer'
                        },
                        success: function(response) {
                            jQuery('.get_ans').val('');

                        }
                    })

        }



        //called when the next button is called
        function handleNextQuestion() {
            checkForAnswer()
               indexNumber++;
               questionNumber++;
                if (indexNumber <= no_q-1) {
                    var total_width = (indexNumber/no_q-1) * 100;
                    total_width = -(total_width);
                    jQuery('.progress-value').attr('style', 'width: '+total_width+'% !important');
                    NextQuestion(indexNumber)
                }
                else {
                    handleEndGame()
                }
        }

        //sets options background back to null after display the right/wrong colors
        function resetOptionBackground() {
            const options = document.getElementsByName("option");
            options.forEach((option) => {
                document.getElementById(option.labels[0].id).style.backgroundColor = ""
            })
        }

        // unchecking all radio buttons for next question(can be done with map or foreach loop also)
        function unCheckRadioButtons() {
            const options = document.getElementsByName("option");
            for (let i = 0; i < options.length; i++) {
                options[i].checked = false;
            }
        }

        // function for when all questions being answered
        function handleEndGame() {
            const playerGrade = (playerScore / no_q) * 100

            jQuery.ajax({
                type : "post",
                url : '<?= admin_url('admin-ajax.php'); ?>',
                data : {
                    action: "quiz_end",
                    quiz_id : quiz_id,
                    playerScore : playerScore,
                    no_q : no_q,
                    playerGrade : playerGrade,
                    time : timer2,
                    type: 'grammer'
                },
                success: function(response) {
                    var data = JSON.parse(response);
                    window.location.href= "<?= home_url() ?>/"+response.url
                    // window.location.href= "</?= home_url() ?>/check-quiz-setting?quiz="+quiz_id
                }
            })

        }

        //closes score modal and resets game
        function closeScoreModal() {
            questionNumber = 1
            playerScore = 0
            wrongAttempt = 0
            indexNumber = 0
            shuffledQuestions = []
            NextQuestion(indexNumber)
            document.getElementById('score-modal').style.display = "none"
        }

        //function to close warning modal
        function closeOptionModal() {
            document.getElementById('option-modal').style.display = "none"
        }
    </script>

<body onload="NextQuestion(0)">
    <main>
<div class="header_section">
   <div class="container_section">
      <h2>Quiz Title (Grammer Quiz)</h2>
      <h3>Quiz Instructions</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec interdum ex vulputate neque euismod vehicula. Proin tincidunt quam tellus, at tempor nunc porta vitae. Aenean venenatis sit amet enim id fringilla. Ut varius mauris in elementum pharetra. Pellentesque sollicitudin suscipit nisl, in pellentesque metus bibendum et.</p>
      <div class="progress">
         <div class="progress-value"></div>
      </div>
   </div>
</div>
</div>
<div class="section_two_container">
   <div class="game-question-container">
      <span>Q<span class='q_no'></span>: </span> 
      <h1 id="display-question"></h1>
   </div>
      <textarea class="get_ans" cols="30" rows="10" placeholder="Answer Here"></textarea>
      <div class="btm_div_time">
         <p> Time:  <div class="countdown"></div></p>
         <!-- <input class="btn-next" type="submit" value="Next"> -->

         <button class="btn-next"  onclick="handleNextQuestion()">Next Question</button>
         <div class="game-details-container">
                    <div> Question : <span id="question-number"></span> / <?= ($questions_in != '') ? $total_count : $total_q ?></div>
                </div>
      </div>
</div>
</main>
    </body>

<style>
    /* Answer QUiz */

.progress {
    background: rgba(255,255,255,0.1);
    justify-content: flex-start;
    border-radius: 100px;
    align-items: center;
    position: relative;
    padding: 0 5px;
    display: flex;
    height: 40px;
    width: 500px;
}

.progress-value {
    animation: load 3s normal forwards;
    box-shadow: 0 10px 40px -10px #fff;
    border-radius: 100px;
    background: #fff;
    height: 30px;

}

@keyframes load {
    0% { width: 0; }
    100% { width: 50%; }
}
.progress {
    margin: auto;
    background: #d7d7d7 !important;
    height: 10px;
    text-align: left;
    justify-content: start;
    padding-left: 0;
    margin-top: 20px;
}

.progress-value {
    height: 10px;
    background: #71c13c;
}
.header_section {
background: #f4f4f4;
font-family: 'Source Sans Pro' !important;
}
.header_section h2 {
font-family: 'Source Sans Pro' !important;
font-size: 42px;
margin: 0;
font-weight: 500 !important;
}

.header_section h3 {
font-family: 'Source Sans Pro' !important;
font-size: 24px;
margin: 0;
font-weight: 400 !important;
}

.header_section p {
color: #707070;
font-size: 14px !important;
margin: 0;
}
.container_section {
max-width: 1170px;
margin: auto;
padding: 70px 0px;
}

.container_section p {
margin-top: 11px;
margin-left: 4px;
}

.header_section h3 {
margin-top: 13px;
}

.progress {
width: 100%;
}
.game-question-container {
display: flex;
}
.game-question-container h1 {
font-size: 32px !important;
font-family: 'Source Sans Pro' !important;
line-height: normal;
font-weight: 600 !important;
}
.game-question-container span {
font-size: 32px;
font-family: 'Source Sans Pro' !important;
margin-top: 9px;
color: #812781;
font-weight: 600 !important;
}
.game-question-container h1 {
margin: 0;
}

.game-question-container {
align-items: center;
}

.section_two_container {
max-width: 774px;
margin: auto;
}

.section_two_container p {
color: #707070;
font-size: 14px !important;
font-family: 'Source Sans Pro' !important;
}

.game-question-container span {
margin: 0;
}
.section_two_container {
margin-top: 50px;
}
.section_two_container textarea {
background-color: rgb(255, 255, 255);
box-shadow: 0px 6px 20px 0px rgba(0, 0, 0, 0.08);
width: 98% !important;
height: 250px;
border: none !important;
padding: 10px;
}
:focus{
outline: none;
}
button.btn-next {
background: #812781 !important;
color: white !important;
background-color: rgb(129, 39, 129);
width: 170px;
height: 50px;
font-family: 'Source Sans Pro' !important;
font-weight: 600 !important;
margin: 24px 0;
border: navajowhite !important;
font-size: 16px;
}
.btm_div_time p {
margin: 0;
}

.btm_div_time {
display: flex;
justify-content: flex-end;
width: 100% !IMPORTANT;
align-items: center;
}

button.btn-next {
margin: 0;
margin-left: 20px;
}

.btm_div_time {
margin-top: 30px;
}

.section_two_container form {
width: 100%;
max-width: unset !important;
}

.section_two_container {
padding-bottom: 50px;
}
i.fa.fa-clock-o {
color: #812781;
}
button.btn-next {
cursor: pointer;
}

/* Answer QUiz */
</style>