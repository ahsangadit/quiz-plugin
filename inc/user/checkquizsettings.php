<?php

get_header();
$user_id = get_current_user_id();




?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap" rel="stylesheet">
<link href="<?php echo plugin_dir_url( __FILE__ )?>../../assets/videoplugin/assets/video.js/dist/video-js.min.css" rel="stylesheet">
<link href="<?php echo plugin_dir_url( __FILE__ )?>../../assets/videoplugin/dist/css/videojs.record.css" rel="stylesheet">

<script src="<?php echo plugin_dir_url( __FILE__ )?>../../assets/videoplugin/assets/video.js/dist/video.min.js"></script>
<script src="<?php echo plugin_dir_url( __FILE__ )?>../../assets/videoplugin/assets/webrtc-adapter/out/adapter.js"></script>
<script src="<?php echo plugin_dir_url( __FILE__ )?>../../assets/videoplugin/dist/videojs.record.js"></script>
<style>
    /* change player background color */
    #myImage {
        background-color: #4d4d4d;
        margin-left: 50px;
    }

    main {
        background: white;
    }

    div#hb-page-title {
        display: none;
    }

    .game-quiz-container {
        margin: auto;
    }
    .game-quiz-container {
        text-align: center;
    }
    .game-options-container span {
        display: flex;
        /*justify-content: center;*/
    }
    .game-options-container span {
        display: flex;
    }

    .game-quiz-container {
        max-width: 740px;
        margin: auto;
    }

    .game-options-container {
        max-width: 390px;
        margin: auto;
        text-align: left !important;
    }
    .game-question-container h1 {
        font-size: 32px !important;
        font-family: 'Source Sans Pro' !important;
        line-height: normal;
        font-weight: 600 !important;
    }

    [type="radio"]:checked,
    [type="radio"]:not(:checked) {
        position: absolute;
        left: -9999px;
    }
    [type="radio"]:checked + label,
    [type="radio"]:not(:checked) + label
    {
        position: relative;
        padding-left: 28px;
        cursor: pointer;
        line-height: 20px;
        display: inline-block;
        color: #666;
    }
    [type="radio"]:checked + label:before,
    [type="radio"]:not(:checked) + label:before {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 13px;
        height: 13px;
        border: 2px solid #812781;
        border-radius: unset;
        background: #fff;
    }
    [type="radio"]:checked + label:after,
    [type="radio"]:not(:checked) + label:after {
        content: '';
        width: 15px;
        height: 15px;
        background: #F87DA9;
        position: absolute;
        top: 4px;
        left: 4px;
        border-radius: 100%;
        -webkit-transition: all 0.2s ease;
        transition: all 0.2s ease;
        content: "\f00c";
        font-family: FontAwesome;
        background: #812781;
        border-radius: 0 !important;
        top: 0;
        left: 0 !important;
        color: white;
        text-align: center;
        display: flex;
        align-items: center;
        justify-content: center;
    }
    [type="radio"]:not(:checked) + label:after {
        opacity: 0;
        -webkit-transform: scale(0);
        transform: scale(0);
    }
    [type="radio"]:checked + label:after {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
    }

    label#option-two-label {}

    .game-options-container label {
        padding-left: 36px !important;
        padding-bottom: 24px;
        text-indent: 0;
        font-family: 'Source Sans Pro' !important;
        font-size: 16px;
        color: #707070 !important;
    }

    .game-options-container {
        max-width: 410px;
    }
    .next-button-container button {
        background: #812781 !important;
        color: white !important;
        background-color: rgb(129, 39, 129);
        width: 170px;
        height: 50px;
        font-family: 'Source Sans Pro' !important;
        font-weight: 600 !important;
        margin: 24px  0;
    }

    .bottom_section_details {
        margin-top: 45px;
        padding-top: 45px;
        border-top: solid 1px #e7e7e7;
    }
    .game-details-container div {font-family: 'Source Sans Pro' !important;font-weight: 600 !important;color: #303030 !important;}

    .game-details-container {
        padding-bottom: 60px;
    }

    .next-button-container button {
        border: none !important;
    }

    span.limit-q {
        display: none !important;
    }
    span.limit-q {
        display: none !important;
    }

    .game-question-container {
        display: flex;
    }

    .game-question-container span {
        font-size: 32px;
        font-family: 'Source Sans Pro' !important;
        margin-top: 9px;
        color: #812781;
        font-weight: 600 !important;
    }

    .game-quiz-container {
        margin-top: 60px;
    }
    .progress {
        background: rgba(255,255,255,0.1);
        justify-content: flex-start;
        border-radius: 100px;
        align-items: center;
        position: relative;
        padding: 0 5px;
        display: flex;
        height: 40px;
        width: 500px;
    }

    .progress-value {
        animation: load 3s normal forwards;
        box-shadow: 0 10px 40px -10px #fff;
        border-radius: 100px;
        background: #fff;
        height: 30px;

    }

    @keyframes load {
        0% { width: 0; }
        100% { width: 0; }
    }
    .progress {
        margin: auto;
        background: #d7d7d7 !important;
        height: 10px;
        text-align: left;
        justify-content: start;
        padding-left: 0;
        margin-top: 20px;
    }

    .progress-value {
        height: 10px;
        background: #71c13c;
    }
    .container_quiz_secton {
        max-width: 672px;
        margin: auto;
        text-align: center;
        font-family: 'Source Sans Pro' !important;
    }


    .container_quiz_secton h2, .container_quiz_secton p, .container_quiz_secton h3 {
        margin: 0;
        padding: 8px;
    }

    .container_quiz_secton p {
        color: #707070 !important;
        font-size: 14px;
        font-weight: 500 !IMPORTANT;
    }

    .game-options-container label {
        line-height: normal !important;
    }

    .game-options-container span {
        align-items: center !important;
    }

    [type="radio"]:checked + label:before, [type="radio"]:not(:checked) + label:before {
        top: 2px;
    }
    .vido-audio-section li {
        background-color: rgb(255, 255, 255);
        box-shadow: 0px 6px 20px 0px rgba(0, 0, 0, 0.08);
        width: 170px;
        height: 50px;
        align-items: center;
        padding: 0px 10px;
        display: flex;
        margin: 0 15px;
        font-family: 'Source Sans Pro' !important;
        cursor: pointer;
    }

    .vido-audio-section ul {
        display: flex;
        align-items: center;
        justify-content: center;
        margin: 0;
        margin-top: 30px;
    }

    .bottom_section_details {
        margin: 0;
        padding: 0;
        border: none !important;
    }
    .vido-audio-section i.fa.fa-check {
        color: white;
        position: absolute;
        right: 9px;
        background: #b2b2b2;
        font-weight: 300 !important;
        width: 15px;
        height: 15px;
        display: flex;
        align-items: center;
        justify-content: center;
        border-radius: 30px;
        top: 7px;
        font-size: 11px;
    }

    .vido-audio-section ul li {
        position: relative;
    }

    [type="radio"]:checked + label:after {
        top: 2px;
        font-size: 12px;
        padding-left: 2px;
    }
    .vido-audio-section i {
        margin-right: 10px;
    }

    i.fa.fa-check {
        margin: 0 !important;
    }

    i.fa.fa-microphone {
        color: #812781;
    }

    i.fa.fa-camera {
        color: #812781;
    }

    i.fa.fa-signal {
        color: #812781;
        font-weight: bold;
    }

    .vido-audio-section {}

    .vido-audio-section ul li.active {}


    .container_quiz_secton h2 {
        font-size: 42px;
        font-weight: normal !important;
    }

    .container_quiz_secton h3 {
        font-size: 28px;
        font-weight: normal;
    }

    .next-button-container button {
        width: 310px;
    }
</style>
<div class="quiz_container_section">
    <div class="container_quiz_secton">
        <h2>Check Your Settings</h2>
        <h3>Instructions</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec interdum ex vulputate neque euismod vehicula. Proin tincidunt quam tellus, at tempor nunc porta vitae. Aenean venenatis sit amet enim id fringilla. Ut varius mauris in elementum pharetra.</p>
        <!-- <div class="progress">
            <div class="progress-value"></div>
        </div> -->
    </div>
    <div class="vido-audio-section">
        <ul>
            <li class="active"><i class="fa fa-microphone" aria-hidden="true"></i>
                Check Mic <i class="fa fa-check check-microphone" aria-hidden="true"></i>
            </li>
            <li><i class="fa fa-camera" aria-hidden="true"></i>
                Check Camera <i class="fa fa-check check-camera" aria-hidden="true"></i>
            </li>
            <li><i class="fa fa-signal" aria-hidden="true"></i>
                Check Internet<i class="fa fa-check check-signal" aria-hidden="true"></i>
            </li>
        </ul>
    </div>
</div>
<div class="bottom_section_details">
    <div class="next-button-container">
        <!--<button class="question_next" >Start Quiz</button>-->
    </div>

</div>
<div class="game-quiz-container">
    <div class="game-options-container">
        <video id="myImage" playsinline class="video-js vjs-default-skin"></video>
    </div>
    <div class="bottom_section_details">
        <div class="next-button-container">
            <button class="question_next" >Start Quiz</button>
        </div>

    </div>

</div>


<?php
get_footer();
?>
<script type="text/javascript">
    function gotStream(stream) {
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        var audioContext = new AudioContext();

        // Create an AudioNode from the stream
        var mediaStreamSource = audioContext.createMediaStreamSource(stream);

        // Connect it to destination to hear yourself
        // or any other node for processing!
        mediaStreamSource.connect(audioContext.destination);
        var video = document.querySelector('video');
        var videoTracks = stream.getVideoTracks();

        jQuery('.check-camera').css('background','#71c13c');
        jQuery('.check-microphone').css('background','#71c13c');
        console.log('permision done');
    }
    function onfail(error) {
        jQuery('.check-camera').css('background','#A20000FF');
        jQuery('.check-microphone').css('background','#A20000FF');
        jQuery('.question_next').prop('disabled', true);
        jQuery('.question_next').css('cursor','not-allowed');
        console.log("permission not granted or system don't have media devices."+error.name);
    }
    navigator.getUserMedia({audio:true,video:true}, gotStream,onfail);
    if(navigator.onLine){
        jQuery('.check-signal').css('background','#71c13c');

    } else {
        jQuery('.check-signal').css('background','#A20000FF');
        jQuery('.question_next').css('cursor','not-allowed');
        jQuery('.question_next').prop('disabled', true);

    }

</script>

<script>
    /* eslint-disable */
    var options = {
        controls: true,
        fluid: false,
        bigPlayButton: false,
        controlBar: {
            volumePanel: false,
            fullscreenToggle: false
        },
        // dimensions of the video.js player
        width: 320,
        height: 280,
        plugins: {
            record: {
                debug: true,
                imageOutputType: 'dataURL',
                imageOutputFormat: 'image/png',
                imageOutputQuality: 0.92,
                image: {
                    // image media constraints: set resolution of camera
                    width: { min: 640, ideal: 640, max: 1280 },
                    height: { min: 480, ideal: 480, max: 920 }
                },
                // dimensions of captured video frames
                frameWidth: 640,
                frameHeight: 480
            }
        }
    };

    var player = videojs('myImage', options, function() {
        // print version information at startup
        var msg = 'Using video.js ' + videojs.VERSION +
            ' with videojs-record ' + videojs.getPluginVersion('record');
        videojs.log(msg);
    });

    // error handling
    player.on('deviceError', function() {
        console.warn('device error:', player.deviceErrorCode);
    });

    player.on('error', function(element, error) {
        console.error(error);
    });

    // snapshot is available
    player.on('finishRecord', function() {
        // the blob object contains the image data that
        // can be downloaded by the user, stored on server etc.

        var data = player.recordedData;

        var fd = new FormData();
        fd.append('fname', 'img.png');
        fd.append('data', data);
        fd.append('action', 'save_img');
        fd.append('quiz_id', '<?php echo $_GET['quiz'] ?>');


        jQuery.ajax({
            type: 'POST',
            url:  '<?= admin_url('admin-ajax.php'); ?>',
            data: fd,
            processData: false,
            contentType: false
        }).done(function(data) {
            jQuery('.question_next').prop('disabled', false);
            jQuery('.question_next').css('cursor','pointer');


        });

    });

    player.on('retry', function() {
        console.log('retry');
    });
    function redirect_video(){

        window.location.href= "<?= home_url() ?>/video-quiz-test?quiz=<?php echo $_GET['quiz'] ?>"

    }
    jQuery(".question_next").on("click", redirect_video);
</script>