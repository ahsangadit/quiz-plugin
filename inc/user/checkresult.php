<?php
header("Cache-Control: no-cache, must-revalidate");
clearstatcache();
get_header();

if(isset($_REQUEST['quiz_id'])){

    $quizid = $_REQUEST['quiz_id'];

    echo $quizid;
    exit();



}
$posts_programs = get_posts([
    'post_type' => 'Programs',
    'post_status' => 'publish',
    'numberposts' => -1
    // 'order'    => 'ASC'
]);

$user = wp_get_current_user();

$assign_students = get_users( ['role' => 'student']);


function datatable_styles()

{
    ?>

    <link rel="stylesheet" href="https://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/1.0.4/css/dataTables.responsive.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.css">
    <script src="https://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        jQuery('table').DataTable();
        jQuery("#open").click(function(){
            jQuery("#a").css("display","block");
            jQuery("#b").css("display","block");
            jQuery('input').val('');
        });
        jQuery(".open_edit").click(function(){
            var id =  jQuery(this).attr("data-id");
            var programid =  jQuery(this).attr("data-programid");
            var test_timelimit =  jQuery(this).attr("data-test_timelimit");
            var test_passinglevel =  jQuery(this).attr("data-test_passinglevel");
            var test_total_marks =  jQuery(this).attr("data-test_total_marks");
            var posttitle =  jQuery(this).attr("data-posttitle");
            var get_assign_students =  jQuery(this).attr("data-get_assign_students");

            jQuery('#test_name').val(posttitle);
            jQuery('#test_programid').val(programid).change();;
            jQuery('#test_timelimit').val(test_timelimit);
            jQuery('#test_passinglevel').val(test_passinglevel);
            jQuery('#test_total_marks').val(test_total_marks);
            jQuery('#assign_students').val(get_assign_students).change();;
            jQuery('#postid').val(id);
            jQuery('#login').val('update');
            console.log(get_assign_students);
            jQuery("#a").css("display","block");
            jQuery("#b").css("display","block");
        });
        jQuery(".cancel").click(function(){
            jQuery("#a").fadeOut();
            jQuery("#b").fadeOut();
        });
        jQuery(document).ready(function($) {
            jQuery('.select2').select2();
        });
        jQuery(".open_delete").click(function(){
            var id =  jQuery(this).attr("data-id");
            var posttitle =  jQuery(this).attr("data-posttitle");
            jQuery('#test_id').val(id);
            jQuery('#test_name').val(posttitle);

            Swal.fire({
                title: 'Do you want to Delete the Test?',
                showCancelButton: true,
                confirmButtonText: 'Confirm Delete',

            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {

                    jQuery.ajax({
                        type : "post",
                        url : '<?= admin_url('admin-ajax.php'); ?>',
                        data : {
                            action: "vgc_agenttest_dlt",
                            id : id,
                            posttitle : posttitle,

                        },
                        beforeSend: function(){
                            jQuery('.vgc_test_tablebody').append('<div class="loader" id="vgc_loader"></div>');
                            jQuery('.vgc_testtr').hide();
                        },
                        success: function(response) {
                            jQuery("#vgc_loader").hide();
                            Swal.fire('Test Deleted!', '', 'success');
                            location.reload();

                        }
                    })

                } else if (result.isDenied) {
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        });
    </script>
    <?php
}
add_action('wp_footer', 'datatable_styles');
if(is_user_logged_in()) {
    $allowed_users = ['agent','administrator'];
    // Get the user object.
    $user = get_userdata(get_current_user_id());
    if(in_array($user->roles[0],$allowed_users)){
        ?>

        <style>

            .main{
                width: 100%;
                height: 100vh;
                text-align: center;
            }

            .main div{
                width: 400px;
                height: 400px;
                margin:0 auto;
                text-align: center;

            }
            .main div button{
                top: 500px;
                height: 30px;
                margin: 0 auto;
            }


            .container_modal{
                display: none;
                width: 100%;
                height: 100vh;
                position: fixed;
                opacity: 0.9;
                background: #222;
                z-index: 40000;
                top:0;
                left: 0;
                overflow: hidden;

                animation-name: fadeIn_Container;
                animation-duration: 1s;

            }

            .modal{
                display:none;
                top: 0;
                min-width: 250px;
                width: 80%;
                height: 400px;
                margin: 0 auto;
                position: fixed;
                z-index: 40001;
                background: #fff;
                border-radius: 10px;
                box-shadow: 0px 0px 10px #000;
                margin-top: 30px;
                margin-left: 10%;

                animation-name: fadeIn_Modal;
                animation-duration: 0.8s;

            }

            .header{
                width: 100%;
                height: 70px;
                border-radius: 10px 10px 0px 0px;
                border-bottom: 2px solid #ccc;
            }

            .header a{
                text-decoration: none;
                float: right;
                line-height: 70px;
                margin-right: 20px;
                color: #aaa;
            }

            .content{
                width: 100%;
                height: 550px;
            }

            form{
                margin-top: 20px;
            }

            form label{
                display: block;
                margin-left: 12%;
                margin-top: 10px;
                font-family: sans-serif;
                font-size: 1rem;
            }

            form input{
                display: block;
                width: 75%;
                margin-left: 12%;
                margin-top: 10px;
                border-radius: 3px;
                font-family: sans-serif;
            }

            #first_label{
                padding-top: 30px;
            }

            #second_label{
                padding-top: 25px;
            }


            .footer_modal{
                width: 100%;
                height: 80px;
                border-radius: 0px 0px 10px 10px;
                border-top: 2px solid #ccc;
            }

            .footer_modal button{
                float: right;
                margin-right: 10px;
                margin-top: 18px;
                text-decoration: none;
            }

            /****MEDIA QUERIES****/

            @media screen and (min-width: 600px){

                .modal{
                    width: 500px;
                    height: 575px;
                    margin-left: calc(46vw - 296px);
                    margin-top: calc(50vh - 290px);
                }


                .header_modal{
                    width: 100%;
                    height: 40px;
                }

                .header_modal a{

                    line-height: 40px;
                    margin-right: 20px;
                    float:right;

                }

                .content{
                    width: 100%;
                    height: 550px;
                    padding-bottom: 20px;
                }

                form label{
                    margin-left: 10%;
                    margin-top: 10px;
                }

                form input {
                    width: 75%;
                    margin-left: 10%;
                    margin-top: 10px;
                }
                form select{
                    width: 75%;
                    margin-left: 10%;
                    margin-top: 10px;
                }

                #first_label{
                    padding-top: 0px;
                }

                #second_label{
                    padding-top: 0px;
                }

                .footer{
                    width: 100%;
                    height: 70px;
                }

                .footer_modal button{
                    float: right;
                    margin-right: 10px;
                    margin-top: 10px;
                }

            }

            /*LARGE SCREEN*/
            @media screen and (min-width: 1300px){

            }

            /****ANIMATIONS****/

            @keyframes fadeIn_Modal {
                from{
                    opacity: 0;
                }
                to{
                    opacity: 1;
                }
            }

            @keyframes fadeIn_Container {
                from{
                    opacity: 0;
                }
                to{
                    opacity: 0.9;
                }
            }

            .modal-body {
                max-height: calc(100vh - 210px);
                overflow-y: auto;
            }

            /*.select2-container {*/
            /*    max-width: 100% !important;*/
            /*    display: block;*/
            /*    float: right;*/
            /*}*/

            /*.select2-close-mask {*/
            /*    z-index: 2099 !important;*/
            /*}*/

            /*.select2-dropdown {*/
            /*    z-index: 3051 !important;*/
            /*}*/

            .loader {
                margin-left: 350%;
                border: 12px solid #f3f3f3;
                border-radius: 50%;
                border-top: 12px solid #3498db;
                width: 50px;
                height: 50px;
                -webkit-animation: spin 2s linear infinite;
                animation: spin 2s linear infinite;
            }

            /* Safari */
            @-webkit-keyframes spin {
                0% { -webkit-transform: rotate(0deg); }
                100% { -webkit-transform: rotate(360deg); }
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }
            #vgc_testcontainer{
                margin-top: 40px;
                padding: 99px;
                background: white;

            }
            button#open {

                margin-top: -30px;
            }

        </style>

        <div class="container" id="vgc_testcontainer">
            <div class="row">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" id="open" style="float:right;">
                    Create Test
                </button>

                <div class="col-xs-12">

                    <table>
                        <thead>
                        <tr>
                            <th>Question</th>
                            <th>Correct Answer</th>
                            <th>Student Answer</th>
                            <th>result</th>

                        </tr>
                        </thead>
                        <tbody class="vgc_test_tablebody">

                        <?php
                        foreach ($get_test as $post){
                            $test_timelimit = get_post_meta( $post->ID, 'test_timelimit', true );
                            $test_passinglevel = get_post_meta( $post->ID, 'test_passinglevel', true );
                            $test_programid = get_post_meta( $post->ID, 'test_programid', true );
                            $test_total_marks = get_post_meta( $post->ID, 'test_total_marks', true );
                            $get_assign_students = get_post_meta( $post->ID, 'assign_students', true );
                            $get_testprogram = get_posts([
                                'ID' => $test_programid,
                                'post_type' => 'Programs',
                                'post_status' => 'publish',
                                'numberposts' => -1
                                // 'order'    => 'ASC'
                            ]);
                            $program_name = ($get_testprogram[0]->post_name) ? $get_testprogram[0]->post_name : '';

                            ?>
                            <tr class="vgc_testtr">
                                <td><?php echo $post->post_title ?></td>
                                <td><?php echo $program_name ?></td>
                                <td><?php echo $test_timelimit ?></td>
                                <td><?php echo $test_passinglevel ?></td>
                                <td><?php echo $test_total_marks ?></td>
                                <td><ul>
                                        <?php

                                        foreach(explode(',', $get_assign_students) as $s){

                                            echo '<li>'.get_user_by( 'id', $s )->user_nicename.'</li>';

                                        }
                                        ?>
                                    </ul>
                                </td>
                                <td>
                                    <button class="btn btn-info open_edit"  data-id="<?php echo $post->ID ?>"
                                            data-posttitle="<?php echo $post->post_title ?>"
                                            data-programid="<?php echo $test_programid ?>"
                                            data-test_timelimit="<?php echo $test_timelimit ?>"
                                            data-test_passinglevel="<?php echo $test_passinglevel ?>"
                                            data-test_total_marks="<?php echo $test_total_marks ?>"
                                            data-get_assign_students="<?php echo $get_assign_students ?>"
                                    >Edit</button>
                                    <button class="btn btn-danger open_delete"
                                            data-id="<?php echo $post->ID ?>"
                                            data-posttitle="<?php echo $post->post_title ?>"
                                    >Delete</button>


                                </td>


                            </tr>

                        <?php } ?>

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
        <div class="container_modal" id="a">

        </div>
        <div class="modal" id="b">
            <div class="header_modal">
                <a href="#" class="cancel">X</a>
            </div>
            <form action="#" method="post">
                <div class="content modal-body">
                    <div class="form-group">
                        <label for="">Test Name</label>
                        <input type="hidden" name="id" id="postid">
                        <input type="text" id="test_name" name="test_name" placeholder="Test Name" required>
                    </div>
                    <div class="form-group">
                        <label for="">Select Programs</label>
                        <select name="test_programid" id="test_programid" class="form-control" required>

                            <option value="">Select Program</option>
                            <?php
                            foreach($posts_programs as $p){

                                ?>
                                <option value="<?php echo $p->ID ?>"  ><?php echo $p->post_title ?></option>
                                <?php

                            }

                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="test_timelimit">Time Limit (in mins)</label>
                        <input type="number" id="test_timelimit"  class="form-control" name="test_timelimit"  min="1"  required>

                    </div>
                    <div class="form-group">
                        <label for="test_timelimit">Passing level</label>
                        <input type="number" id="test_passinglevel"  class="form-control" name="test_passinglevel" min="1" required>
                    </div>
                    <div class="form-group">
                        <label for="test_total_marks">Total Marks</label>
                        <input type="number" id="test_total_marks"  class="form-control" name="test_total_marks"  min="1" required>

                    </div>
                    <div class="form-group">
                        <label for="">Select Student</label>
                        <select name="assign_students[]" id="assign_students" class="form-control" required >
                            <?php
                            foreach($assign_students as $a){

                                ?>
                                <option value="<?php echo $a->ID ?>"  ><?php echo $a->user_nicename ?></option>
                                <?php

                            }

                            ?>
                        </select>
                    </div>


                </div>
                <div class="footer_modal">
                    <button type="submit" id="login" name="vgc_create_test_agent" value="save">Submit </button>
                    <button class="cancel">cancel</button>
                </div>
            </form>
        </div>
        <?php

    }
    else{

        global $wp_query;
        $wp_query->set_404();
        status_header( 404 );
        get_template_part( 404 ); exit();
    }
}
else{

    global $wp_query;
    $wp_query->set_404();
    status_header( 404 );
    get_template_part( 404 ); exit();


}
get_footer();


?>