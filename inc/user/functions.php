<?php
add_filter( 'page_template', 'vgc_student_checkresult_template' );
function vgc_student_checkresult_template( $page_template )
{
    if ( is_page( 'student-check-result' ) ) {

        $page_template =  plugin_dir_path( __DIR__ ). '/user/checkresult.php';
    }

    return $page_template;
}