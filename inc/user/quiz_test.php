<?php

get_header();
if(isset($_GET['quiz'])){
    $quiz_id = $_GET['quiz'];
}

$user_id = get_current_user_id();
$args = array(
    'numberposts'   => -1,
    'post_type'     => 'quizresult',
    'meta_query'    => array(
        'relation'      => 'AND',
        array(
            'key'       => 'quiz_id',
            'compare'   => '=',
            'value'     => $quiz_id,
        ),
        array(
            'key'       => 'user',
            'compare'   => '=',
            'value'     => $user_id,
        )
    )
);
$result_id = get_posts($args);

$questions_in = get_post_meta( $result_id[0]->ID, 'question_ids', true );

if($questions_in == ''){
    $questions_in = [];
}
// var_dump($questions_in);
$get_time = get_post_meta($result_id[0]->ID , 'time', true);
$complete_time = get_post_meta($result_id[0]->ID , 'complete_time', true);

function video_pluginscripts()

{
    ?>

    <link href="<?php echo plugin_dir_url( __FILE__ )?>../../assets/videoplugin/assets/video.js/dist/video-js.min.css" rel="stylesheet">
    <link href="<?php echo plugin_dir_url( __FILE__ )?>../../assets/videoplugin/dist/css/videojs.record.css" rel="stylesheet">
    <link href="<?php echo plugin_dir_url( __FILE__ )?>../../assets/videoplugin/assets/css/examples.css" rel="stylesheet">

    <script src="<?php echo plugin_dir_url( __FILE__ )?>../../assets/videoplugin/assets/video.js/dist/video.min.js"></script>
    <script src="<?php echo plugin_dir_url( __FILE__ )?>../../assets/videoplugin/assets/recordrtc/RecordRTC.js"></script>
    <script src="<?php echo plugin_dir_url( __FILE__ )?>../../assets/videoplugin/assets/webrtc-adapter/out/adapter.js"></script>
    <script src="<?php echo plugin_dir_url( __FILE__ )?>../../assets/videoplugin/assets/fine-uploader/fine-uploader/fine-uploader.js"></script>
    <script src="<?php echo plugin_dir_url( __FILE__ )?>../../assets/videoplugin/dist/videojs.record.js"></script>


    <script>
        /* eslint-disable */
        /* workaround browser issues */

        var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
        var isEdge = /Edge/.test(navigator.userAgent);
        var isOpera = !!window.opera || navigator.userAgent.indexOf('OPR/') !== -1;

        function applyAudioWorkaround() {
            if (isSafari || isEdge) {
                if (isSafari && window.MediaRecorder !== undefined) {
                    // this version of Safari has MediaRecorder
                    // but use the only supported mime type
                    options.plugins.record.audioMimeType = 'audio/mp4';
                } else {
                    // support recording in safari 11/12
                    // see https://github.com/collab-project/videojs-record/issues/295
                    options.plugins.record.audioRecorderType = StereoAudioRecorder;
                    options.plugins.record.audioSampleRate = 44100;
                    options.plugins.record.audioBufferSize = 4096;
                    options.plugins.record.audioChannels = 2;
                }

                console.log('applied audio workarounds for this browser');
            }
        }

        function applyVideoWorkaround() {
            // use correct video mimetype for opera
            if (isOpera) {
                options.plugins.record.videoMimeType = 'video/webm\;codecs=vp8'; // or vp9
            }
        }

        function applyScreenWorkaround() {
            // Polyfill in Firefox.
            // See https://blog.mozilla.org/webrtc/getdisplaymedia-now-available-in-adapter-js/
            if (adapter.browserDetails.browser == 'firefox') {
                adapter.browserShim.shimGetDisplayMedia(window, 'screen');
            }
        }

    </script>

    <style>
        /* change player background color */
        #myVideo {
            background-color: #9ab87a;
        }
    </style>

    <?php
}
add_action('wp_footer', 'video_pluginscripts');


$mcqs_category_id = get_post_meta( $quiz_id, 'mcqs_category_id', true );
$questions = [];
$args = array(
    'post_type'     => 'questions',
    'meta_query' => array(
        array(
            'key' => 'questions_type',
            'value' => 'mcqs',
        ),
        array(
            'key' => 'question_category',
            'value' => $mcqs_category_id,
        ),
    ),
    'orderby' => 'rand',
    'posts_per_page' => -1,
);

$questions = get_posts( $args );
$questions_get = [];

if($user_id != 0){
    foreach($questions as $key => $val){

        if( (!in_array( $val->ID, $questions_in) ) ){
            $questions_get[$key]['question_id'] = $val->ID;
            $questions_get[$key]['question'] = $val->post_title;
            $questions_get[$key]['question_timelimit'] = get_post_meta( $val->ID, 'question_timelimit', true );
            $questions_get[$key]['program_id'] = get_post_meta( $val->ID, 'questions_programid', true );
            $questions_get[$key]['optionA'] = get_post_meta( $val->ID, 'option1', true );
            $questions_get[$key]['optionB'] = get_post_meta( $val->ID, 'option2', true );
            $questions_get[$key]['optionC'] = get_post_meta( $val->ID, 'option3', true );
            $questions_get[$key]['optionD'] = get_post_meta( $val->ID, 'option4', true );
            $questions_get[$key]['correctOption'] = get_post_meta( $val->ID, 'correct_ans', true );
        }
    }
}

$test_timelimit = get_post_meta( $quiz_id, 'mcqs_time', true );
$test_passinglevel = 100;
$test_programid = get_post_meta( $quiz_id, 'test_programid', true );
$test_total_marks = get_post_meta( $quiz_id, 'test_total_marks', true );
$total_q = count($questions);
$total_q_get =  count($questions_in);
if($questions_in != ''){
    $total_count = $total_q - $total_q_get;
    $questions_get = array_values($questions_get);

}
$nonce = wp_create_nonce("time");

if($complete_time){ ?>
    <script>
        window.location.href= "<?= home_url() ?>/check-quiz-setting?quiz=<?= $_GET['quiz'] ?>"
    </script>

    <?php
}

if($total_count == 0){ ?>
    <script>
        window.location.href= "<?= home_url() ?>/check-quiz-setting?quiz=<?= $_GET['quiz'] ?>"
        </script>
    <?php
    }

?>

    <script>
        const questions =  <?= json_encode($questions_get); ?>;
        const quiz_id = <?= $quiz_id ?>;
        const user_id = <?= $user_id ?>;
        const no_q =  <?= ($questions_in != '') ? $total_count : $total_q ?>;
        //const no_q =  <//=  $total_q ?>;
        const test_time =  "<?= $get_time == '' ? $test_timelimit.":00" : $get_time ?>";
        const program_id =  <?= $test_programid ?>;
        const passing_range =  <?= $test_passinglevel ?>;

        //    var timer2 = test_time+":01";
        var timer2 = test_time;
        var interval = setInterval(function() {
            var timer = timer2.split(':');
            //by parsing integer, I avoid all extra string processing
            var minutes = parseInt(timer[0], 10);
            var seconds = parseInt(timer[1], 10);
            --seconds;
            minutes = (seconds < 0) ? --minutes : minutes;
            if (minutes < 0) clearInterval(interval);
            seconds = (seconds < 0) ? 59 : seconds;
            seconds = (seconds < 10) ? '0' + seconds : seconds;
            jQuery('.countdown').html(minutes + ':' + seconds);
            timer2 = minutes + ':' + seconds;
        }, 1000);

        function get_time(){
            jQuery.ajax({
                type: "POST",
                url:  '<?= admin_url('admin-ajax.php'); ?>',
                data : {
                    action: "get_time_data",
                    quiz_id : quiz_id,
                    time: timer2
                },
            }).success(function(response){
                var data = JSON.parse(response);
                console.log(data.time);
            });
        }

        setInterval(get_time, 60000);



        let shuffledQuestions = [] //empty array to hold shuffled selected questions

        function handleQuestions() {
            //function to shuffle and push 10 questions to shuffledQuestions array
            while ( shuffledQuestions.length <= no_q-1 ) {
                const random = questions[Math.floor(Math.random() * questions.length)]
                if (!shuffledQuestions.includes(random)) {
                    shuffledQuestions.push(random)
                }
            }
        }


        let questionNumber = 1
        let playerScore = 0
        let wrongAttempt = 0
        let indexNumber = 0
        var interval = '';
        // function for displaying next question in the array to dom
        function NextQuestion(index) {

            handleQuestions()
            const currentQuestion = shuffledQuestions[index]
            console.log(questionNumber);

            document.getElementById("question-number").innerHTML = questionNumber
            jQuery(".q_no").html(questionNumber)
            // document.getElementById("player-score").innerHTML = playerScore
            document.getElementById("display-question").innerHTML = currentQuestion.question;
            document.getElementById("option-one-label").innerHTML = currentQuestion.optionA;
            document.getElementById("option-two-label").innerHTML = currentQuestion.optionB;
            document.getElementById("option-three-label").innerHTML = currentQuestion.optionC;
            document.getElementById("option-four-label").innerHTML = currentQuestion.optionD;

            jQuery('.question_next').attr('data-question_no',questionNumber);
            jQuery('.question_next').attr('data-question',currentQuestion.question_id);
            jQuery('#option-one').val(currentQuestion.optionA);
            jQuery('#option-two').val(currentQuestion.optionB);
            jQuery('#option-three').val(currentQuestion.optionC);
            jQuery('#option-four').val(currentQuestion.optionD);

            if(currentQuestion.question_timelimit != ''){
                jQuery('.limit-q').css('display','block');
                var timer2 = '';
                timer2 = currentQuestion.question_timelimit+":01";
                console.log(currentQuestion);

                    interval = setInterval(function() {
                    var timer = '';
                     timer = timer2.split(':');
                    //by parsing integer, I avoid all extra string processing
                    var minutes = '';
                    var seconds = '';
                    minutes = parseInt(timer[0], 10);
                    seconds = parseInt(timer[1], 10);
                    --seconds;
                    minutes = (seconds < 0) ? --minutes : minutes;
                    if (minutes < 0) clearInterval(interval);
                    seconds = (seconds < 0) ? 59 : seconds;
                    seconds = (seconds < 10) ? '0' + seconds : seconds;
                    jQuery('.limit').html('');
                    jQuery('.limit').html(minutes + ':' + seconds);
                    timer2 = minutes + ':' + seconds;
                    console.log('yehi',timer2)
                    if(minutes == '0' && seconds == '00'){

                        handleNextQuestion();
                    }
                }, 1000);
            }else{
                jQuery('.limit-q').css('display','none');
            }


        }


        function checkForAnswer() {
            const currentQuestion = shuffledQuestions[indexNumber] //gets current Question
            //  console.log(currentQuestion);
            const currentQuestionAnswer = currentQuestion.correctOption //gets current Question's answer
            // console.log(currentQuestionAnswer);
            const options = document.getElementsByName("option"); //gets all elements in dom with name of 'option' (in this the radio inputs)
            let correctOption = null
            options.forEach((option) => {
                if (option.value === currentQuestionAnswer) {
                    //get's correct's radio input with correct answer
                    correctOption = option.labels[0].id
                }
            })

            //checking to make sure a radio input has been checked or an option being chosen
            if (options[0].checked === false && options[1].checked === false && options[2].checked === false && options[3].checked == false) {
                //   document.getElementById('option-modal').style.display = "flex"
                jQuery.ajax({
                    type : "post",
                    url : '<?= admin_url('admin-ajax.php'); ?>',
                    data : {
                        action: "save_result_res",
                        quiz_id : quiz_id,
                        type:'mcqs',
                        answer : '',
                        question : currentQuestion.question,
                        question_id : currentQuestion.question_id,
                        correct_ans : currentQuestionAnswer,
                        time : timer2,
                        question_no: questionNumber-1
                    },
                    success: function(response) {
                        //   playerScore++
                        indexNumber++
                        questionNumber++
                    }
                })
            }

            //checking if checked radio button is same as answer
            options.forEach((option) => {
                //   && option.value === currentQuestionAnswer
                if (option.checked === true && option.value === currentQuestionAnswer) {
                    playerScore++
                    indexNumber++
                    questionNumber++
                }
                else if (option.checked && option.value !== currentQuestionAnswer) {
                    const wrongLabelId = option.labels[0].id
                    wrongAttempt++
                    indexNumber++
                    questionNumber++
                }
                if(option.checked === true ){
                    jQuery.ajax({
                        type : "post",
                        url : '<?= admin_url('admin-ajax.php'); ?>',
                        data : {
                            action: "save_result_res",
                            type:'mcqs',
                            quiz_id : quiz_id,
                            answer : option.value,
                            question : currentQuestion.question,
                            question_id : currentQuestion.question_id,
                            correct_ans : currentQuestionAnswer,
                            time : timer2,
                            question_no: questionNumber-1
                        },
                        success: function(response) {
                        }
                    })
                }
            })
        }



        //called when the next button is called
        function handleNextQuestion() {
            checkForAnswer()
            unCheckRadioButtons()
            //delays next question displaying for a second
            setTimeout(() => {
                if (indexNumber <= no_q-1) {
                    console.log('here');
                    console.log(indexNumber);
                    console.log(no_q);
                    var total_width = (indexNumber/no_q-1) * 100;
                    total_width = 100 - (-(total_width));
                    jQuery('.progress-value').attr('style', 'width: '+total_width+'% !important');
                    console.log('total_width',total_width);
                    clearInterval(interval);
                    NextQuestion(indexNumber)
                }
                else {
                    handleEndGame()
                }
                resetOptionBackground()
            }, 1000);

        }

        //sets options background back to null after display the right/wrong colors
        function resetOptionBackground() {
            const options = document.getElementsByName("option");
            options.forEach((option) => {
                document.getElementById(option.labels[0].id).style.backgroundColor = ""
            })
        }

        // unchecking all radio buttons for next question(can be done with map or foreach loop also)
        function unCheckRadioButtons() {
            const options = document.getElementsByName("option");
            for (let i = 0; i < options.length; i++) {
                options[i].checked = false;
            }
        }

        // function for when all questions being answered
        function handleEndGame() {
            let remark = null
            let remarkColor = null

            // condition check for player remark and remark color
            if (playerScore <= 3) {
                // remark = "Bad Grades, Keep Practicing."
                // remarkColor = "red"
                remark = "Thank You"
                remarkColor = "green"
            }
            else if (playerScore >= 4 && playerScore < 7) {
                // remark = "Average Grades, You can do better."
                // remarkColor = "orange"
                remark = "Thank You"
                remarkColor = "green"
            }
            else if (playerScore >= 7) {
                // remark = "Excellent, Keep the good work going."
                // remarkColor = "green"
                remark = "Thank You"
                remarkColor = "green"
            }


            const playerGrade = (playerScore / no_q) * 100

            jQuery.ajax({
                type : "post",
                url : '<?= admin_url('admin-ajax.php'); ?>',
                data : {
                    action: "quiz_end",
                    quiz_id : quiz_id,
                    playerScore : playerScore,
                    no_q : no_q,
                    playerGrade : playerGrade,
                    time : timer2,
                    mcqtest: 1,
                    type: 'mcqs'
                },
                success: function(response) {
                    var data = JSON.parse(response);
                    window.location.href= "<?= home_url() ?>/"+data.url
                }
            })
        }

        //closes score modal and resets game
        function closeScoreModal() {
            questionNumber = 1
            playerScore = 0
            wrongAttempt = 0
            indexNumber = 0
            shuffledQuestions = []
            NextQuestion(indexNumber)
            document.getElementById('score-modal').style.display = "none"
        }

        //function to close warning modal
        function closeOptionModal() {
            document.getElementById('option-modal').style.display = "none"
        }
    </script>
    <style>


        main {
            background: white;
        }

        div#hb-page-title {
            display: none;
        }

        .game-quiz-container {
            margin: auto;
        }
        .game-quiz-container {
            text-align: center;
        }
        .game-options-container span {
            display: flex;
            /*justify-content: center;*/
        }
        .game-options-container span {
            display: flex;
        }

        .game-quiz-container {
            max-width: 740px;
            margin: auto;
        }

        .game-options-container {
            max-width: 390px;
            margin: auto;
            text-align: left !important;
        }
        .game-question-container h1 {
            font-size: 32px !important;
            font-family: 'Source Sans Pro' !important;
            line-height: normal;
            font-weight: 600 !important;
        }

        [type="radio"]:checked,
        [type="radio"]:not(:checked) {
            position: absolute;
            left: -9999px;
        }
        [type="radio"]:checked + label,
        [type="radio"]:not(:checked) + label
        {
            position: relative;
            padding-left: 28px;
            cursor: pointer;
            line-height: 20px;
            display: inline-block;
            color: #666;
        }
        [type="radio"]:checked + label:before,
        [type="radio"]:not(:checked) + label:before {
            content: '';
            position: absolute;
            left: 0;
            top: 0;
            width: 13px;
            height: 13px;
            border: 2px solid #812781;
            border-radius: unset;
            background: #fff;
        }
        [type="radio"]:checked + label:after,
        [type="radio"]:not(:checked) + label:after {
            content: '';
            width: 15px;
            height: 15px;
            background: #F87DA9;
            position: absolute;
            top: 4px;
            left: 4px;
            border-radius: 100%;
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease;
            content: "\f00c";
            font-family: FontAwesome;
            background: #812781;
            border-radius: 0 !important;
            top: 0;
            left: 0 !important;
            color: white;
            text-align: center;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        [type="radio"]:not(:checked) + label:after {
            opacity: 0;
            -webkit-transform: scale(0);
            transform: scale(0);
        }
        [type="radio"]:checked + label:after {
            opacity: 1;
            -webkit-transform: scale(1);
            transform: scale(1);
        }

        label#option-two-label {}

        .game-options-container label {
            padding-left: 36px !important;
            padding-bottom: 24px;
            text-indent: 0;
            font-family: 'Source Sans Pro' !important;
            font-size: 16px;
            color: #707070 !important;
        }

        .game-options-container {
            max-width: 410px;
        }
        .next-button-container button {
            background: #812781 !important;
            color: white !important;
            background-color: rgb(129, 39, 129);
            width: 170px;
            height: 50px;
            font-family: 'Source Sans Pro' !important;
            font-weight: 600 !important;
            margin: 24px  0;
        }

        .bottom_section_details {
            margin-top: 45px;
            padding-top: 45px;
            border-top: solid 1px #e7e7e7;
        }
        .game-details-container div {font-family: 'Source Sans Pro' !important;font-weight: 600 !important;color: #303030 !important;}

        .game-details-container {
            padding-bottom: 60px;
        }

        .next-button-container button {
            border: none !important;
        }

        /*span.limit-q {*/
        /*    display: none !important;*/
        /*}*/
        /*span.limit-q {*/
        /*    display: none !important;*/
        /*}*/

        .game-question-container {
            display: flex;
        }

        .game-question-container span {
            font-size: 32px;
            font-family: 'Source Sans Pro' !important;
            margin-top: 9px;
            color: #812781;
            font-weight: 600 !important;
        }

        .game-quiz-container {
            margin-top: 60px;
        }
        .progress {
            background: rgba(255,255,255,0.1);
            justify-content: flex-start;
            border-radius: 100px;
            align-items: center;
            position: relative;
            padding: 0 5px;
            display: flex;
            height: 40px;
            width: 500px;
        }

        .progress-value {
            animation: load 3s normal forwards;
            box-shadow: 0 10px 40px -10px #fff;
            border-radius: 100px;
            background: #fff;
            height: 30px;

        }

        @keyframes load {
            0% { width: 0; }
            100% { width: 0; }
        }
        .progress {
            margin: auto;
            background: #d7d7d7 !important;
            height: 10px;
            text-align: left;
            justify-content: start;
            padding-left: 0;
            margin-top: 20px;
        }

        .progress-value {
            height: 10px;
            background: #71c13c;
        }
        .countdown {

            font-weight: bolder!important;
            color: black!important;

        }
    </style>
    <body onload="NextQuestion(0)">
    <main>
        <!-- creating a modal for when quiz ends -->
        <div class="modal-container" id="score-modal">

            <div class="modal-content-container">

                <h1>Congratulations, Quiz Completed.</h1>

                <div class="grade-details">
                    <p>Wrong Answers : <span id="wrong-answers"></span></p>
                    <p>Right Answers : <span id="right-answers"></span></p>
                    <p>Grade : <span id="grade-percentage"></span>%</p>
                    <p ><span id="remarks"></span></p>
                </div>

                <div class="modal-button-container">
                    <button onclick="closeScoreModal()">Continue</button>
                </div>

            </div>
        </div>
        <div class="quiz_container_section">
            <div class="container">
                <h2>Quiz Title (Multiple Choice Quiz)</h2>
                <h3>Quiz Instructions</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec interdum ex vulputate neque euismod vehicula. Proin tincidunt quam tellus, at tempor nunc porta vitae. Aenean venenatis sit amet enim id fringilla. Ut varius mauris in elementum pharetra.</p>
                <div class="progress">
                    <div class="progress-value"></div>
                </div>
            </div>
        </div>
        <div class="game-quiz-container">



            <div class="game-question-container">
            <span>Q<span class='q_no'></span>: </span>  <h1 id="display-question"> </h1>
                <span class="limit-q">
                    <label for="question-limit" class="limit" id="question-limit"></label>
                </span>
            </div>

            <div class="game-options-container">

                <div class="modal-container" id="option-modal">

                    <div class="modal-content-container">
                        <h1>Please Pick An Option</h1>

                        <div class="modal-button-container">
                            <button onclick="closeOptionModal()">Continue</button>
                        </div>

                    </div>

                </div>

                <!--                <video id="myVideo" playsinline class="video-js vjs-default-skin"></video>-->

                <span>
                    <input type="radio" id="option-one" name="option" class="radio" value="optionA" />
                    <label for="option-one" class="option" id="option-one-label"></label>
                </span>


                <span>
                    <input type="radio" id="option-two" name="option" class="radio" value="optionB" />
                    <label for="option-two" class="option" id="option-two-label"></label>
                </span>


                <span>
                    <input type="radio" id="option-three" name="option" class="radio" value="optionC" />
                    <label for="option-three" class="option" id="option-three-label"></label>
                </span>


                <span>
                    <input type="radio" id="option-four" name="option" class="radio" value="optionD" />
                    <label for="option-four" class="option" id="option-four-label"></label>
                </span>



            </div>
            <div class="bottom_section_details">
                <div class="countdown"></div>
                <div class="next-button-container">
                    <button class="question_next"  onclick="handleNextQuestion()">Next Question</button>
                </div>
                <div class="game-details-container">
                    <div> Question : <span id="question-number"></span> / <?= ($questions_in != '') ? $total_count : $total_q ?></div>
                </div>
            </div>

        </div>
    </main>
    </body>

<?php

get_footer();
