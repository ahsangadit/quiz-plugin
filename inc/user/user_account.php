<?php

get_header();
$user_id = get_current_user_id();
$posts = [];
$args = array(
    'post_type'     => 'tests'
);

$postlist = get_posts( $args );
//var_dump($postlist);
foreach($postlist as $val){
    $data = get_post_meta($val->ID, 'assign_students', true);
    $data = explode(',',$data);
    if(in_array($user_id, $data)){
        $posts[] = $val;
    }
}
$nonce = wp_create_nonce("quiz");
$getuser = get_user_by( 'id', $user_id );
$getuser_meta = get_user_meta($user_id);



?>
<style>
    table {
        width: 750px;
        border-collapse: collapse;
        margin:50px auto;
        margin-left: 24%!important;
    }

    /* Zebra striping */
    tr:nth-of-type(odd) {
        background: #eee;
    }

    th {
        background: #3498db;
        color: white;
        font-weight: bold;
    }

    td, th {
        padding: 10px;
        border: 1px solid #ccc;
        text-align: left;
        font-size: 18px;
    }

    /*
    Max width before this PARTICULAR table gets nasty
    This query will take effect for any screen smaller than 760px
    and also iPads specifically.
    */
    @media
    only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {

        table {
            width: 100%;
        }

        /* Force table to not be like tables anymore */
        table, thead, tbody, th, td, tr {
            display: block;
        }

        /* Hide table headers (but not display: none;, for accessibility) */
        thead tr {
            position: absolute;
            top: -9999px;
            left: -9999px;
        }

        tr { border: 1px solid #ccc; }

        td {
            /* Behave  like a "row" */
            border: none;
            border-bottom: 1px solid #eee;
            position: relative;
            padding-left: 50%;
        }

        td:before {
            /* Now like a table header */
            position: absolute;
            /* Top/left values mimic padding */
            top: 6px;
            left: 6px;
            width: 45%;
            padding-right: 10px;
            white-space: nowrap;
            /* Label the data */
            content: attr(data-column);

            color: #000;
            font-weight: bold;
        }

    }

    /* From cssbuttons.io */
    .btn-success {
        padding: 1.3em 3em;
        font-size: 12px;
        text-transform: uppercase;
        letter-spacing: 2.5px;
        font-weight: 500;
        color: #000;
        background-color: #fff;
        border: none;
        border-radius: 45px;
        box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
        transition: all 0.3s ease 0s;
        cursor: pointer;
        outline: none;
    }

    .btn-success:hover {
        background-color: #2EE59D;
        box-shadow: 0px 15px 20px rgba(46, 229, 157, 0.4);
        color: #fff;
        transform: translateY(-7px);
    }

    .btn-success:active {
        transform: translateY(-1px);
    }
    .btn-danger {
        padding: 1.3em 3em;
        font-size: 12px;
        text-transform: uppercase;
        letter-spacing: 2.5px;
        font-weight: 500;
        color: #ffffff;
        background-color: #073f65;
        border: none;
        border-radius: 45px;
        box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
        transition: all 0.3s ease 0s;
        cursor: pointer;
        outline: none;
        width: 150px!important;
    }

    .btn-danger:hover {
        background-color: #136293;
        box-shadow: 0px 15px 20px rgb(7, 63, 101);
        color: #fff;
        transform: translateY(-7px);
    }

    .btn-danger:active {
        transform: translateY(-1px);
    }
</style>
<style>
    .container_section {
        max-width: 1170px;
        margin: auto;
    }

    .student_name_row {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .student_name h2 {font-family: 'Source Sans Pro' !important;font-size: 42px;font-weight: 500 !important;}

    .request_btn a {
        background-color: rgb(129, 39, 129);
        width: 193px;
        height: 50px;
        display: flex;
        align-items: center;
        justify-content: center;
        color: white !important;
        text-decoration: none;
        font-family: 'Source Sans Pro' !important;
    }

    .request_btn {}

    .student_information ul {
        padding: 0;
        margin: 0;
        list-style: none;
        display: flex;
    }

    .student_information ul li {
        padding: 0 25px !important;
    }

    .student_name h2 {
        margin: 0;
    }

    .student_information {
        margin-top: 20px;
    }

    p.dec_for_student {
        color: #707070 !important;
        font-family: 'Source Sans Pro' !important;
        line-height: 1.5;
        font-size: 14px;
        margin-top: 20px;
    }


    .student_information li a {
        font-family: 'Source Sans Pro';
        font-size: 14px;
        color: #404040;
        text-decoration: none;
    }

    ul.quiz_head {
        padding: 0;
        margin: 0;
        list-style: none;
    }

    ul.quiz_body {
        padding: 0;
        list-style: none;
        font-family: 'Source Sans Pro';
    }

    ul.quiz_head {
        font-family: 'Source Sans Pro';
    }
    ul.quiz_body li {
        display: flex;
    }


    .quiz_section ul li span:first-child {
        width: 374px !important;
        display: inline-block;
    }

    .quiz_section ul li span {
        width: 162px;
        display: inline-block;
    }
    .comment_section {
        font-family: 'Source Sans Pro';
    }
    .quiz_head table.table {
        margin: auto;
        max-width: 100%;
        border-collapse: collapse;
        width: 100%;
        text-align: left;
    }



    .quiz_head  table.table tbody td {
        background: #f4f4f4;
        padding: 8px 5px;
        margin-bottom: 10px !important;
        /* display: block; */
        font-size: 14px;
    }

    .quiz_head  table.table tr {
        border-bottom: solid 10px white;
    }

    .quiz_head  table.table tr {
        margin-bottom: 10px !important;
        width: 100%;
    }

    .quiz_head {
        font-family: 'Source Sans Pro';
    }

    .quiz_head  table.table tbody td {
        margin-bottom: 10px;
    }


    .quiz_head tbody tr td:last-child {
        background: white;
        text-align: center;
        padding: 0;
        padding-left: 10px;
    }

    .comment_details {
        padding-bottom: 20px;
        border-bottom: solid 1px #d7d7d7;
        margin: 20px 0;
    }



    .comment_section textarea {
        width: 100%;
    }

    .quiz_head tbody tr td:last-child a {
        background: #838383;
        display: block;
        padding: 9px;
        text-decoration: none;
        color: white;
    }

    .quiz_head tbody tr td:last-child a:hover {
        background: #72c13d;
    }

    table.table tbody td:first-child {
        font-weight: 600 !important;
    }

    .quiz_head th {
        color: #812781;
        font-family: 'Source Sans Pro' !important;
        text-transform: uppercase;
        font-size: 14px;
    }



    .quiz_head table.table {
        margin-top: 25px;
    }
    .quiz_head table.table {
        margin: auto !important;
    }

    .content-area {
        background: white;
    }

    .quiz_head th, .quiz_head td {
        border: navajowhite;
    }

    .quiz_head table.table {
        border: navajowhite;
    }

    .quiz_head th {
        background: white !important;
    }
    div#hb-page-title {
        display: none;
    }
    .student_information i {
        position: absolute;
        left: 0;
        top: 4px;
    }

    .student_information ul li {
        position: relative;
        padding-left: 19px !important;
    }

    .comment_section textarea {
        width: 100%;
        background-color: rgb(255, 255, 255);
        box-shadow: 0px 6px 20px 0px rgb(0 0 0 / 8%);
        border: navajowhite;
        height: 160px;
    }
    .student_information i {
        color: #812781;
        font-weight: bold !important;
    }

    .student_information li a {
        font-weight: 600 !important;
    }
    input.comment_submit_btn {
        background-color: rgb(129, 39, 129);
        width: 120px;
        height: 46px;
        box-shadow: unset !important;
        position: absolute;
        right: 0;
        z-index: 11;
        bottom: 17px;
        right: 20px;
    }



    .comment_section form {
        position: relative;
    }

    .container_section {
        padding-top: 55px;
    }
    .complete-btn{

        background: #71c13c!important;
    }
    .quiz_section {
        margin-bottom: 70px;
    }
    a.confirms.btn.btn__active.btn-startquiz {
        background: #812781;
    }
</style>
<div id="primary" class="content-area">




    <div class="container_section">
        <div class="student_name_row">
            <div class="student_name">
                <h2>Welcome <?php echo $getuser->user_nicename ?></h2>
            </div>
            <div class="request_btn">
<!--                <a href="#">Send Quiz Invite</a>-->
            </div>

        </div>
        <div class="student_information">
            <ul>
                <li><a href="mailto:<?php echo $getuser->user_email ?>"><i class="fa fa-envelope-o" aria-hidden="true"></i><?php echo $getuser->user_email ?></a></li>
                <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i><?php echo $getuser_meta['cp_location'][0] ?></a></li>
                <li><a href="#"><i class="fa fa-calendar" aria-hidden="true"></i><?php echo $getuser->user_registered ?></a></li>
                <li><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $getuser_meta['cp_location'][0] ?></a></li>

            </ul>
            <p class="dec_for_student">
                <?php echo $getuser_meta['description'][0]; ?>
            </p>
        </div>
        <div class="student_name_row">
            <div class="student_name">
                <h2>Quiz Invitations</h2>
            </div>
            <div class="request_btn">
                <!--                <a href="#">Send Quiz Invite</a>-->
            </div>

        </div>
        <div class="quiz_section">
            <div class="quiz_head">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Quiz Title</th>
                        <th>Status</th>
                        <th>Start Date</th>
                        <th>Due Date</th>
                        <th>Quiz Type</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    if($user_id != 0 && count($posts) > 0){

                        foreach($posts as $val){
                            $quiz_test_id = get_option( 'quiz_test' );
                            $url =  get_permalink( $quiz_test_id );
                            $quiz_id = $val->ID;
                            $user_id = get_current_user_id();
                            $args = array(
                                'numberposts'   => -1,
                                'post_type'     => 'quizresult',
                                'meta_query'    => array(
                                    'relation'      => 'AND',
                                    array(
                                        'key'       => 'quiz_id',
                                        'compare'   => '=',
                                        'value'     => $quiz_id,
                                    ),
                                    array(
                                        'key'       => 'user',
                                        'compare'   => '=',
                                        'value'     => $user_id,
                                    )
                                )
                            );
                            $get_result = get_posts($args);
                            $percentage = get_post_meta($get_result[0]->ID , 'percentage' ,true );
                            $mcqs_end = get_post_meta($get_result[0]->ID , 'mcqs_end' ,true );
                            $video_end = get_post_meta($get_result[0]->ID , 'video_end' ,true );

                            if(empty($mcqs_end) ||  empty($video_end)){
                            ?>
                            <tr>
                                <td data-label="Contact ID"><?= $val->post_title ?></td>

                               
                                    <td data-label="Power">Inprogress</td>

                                
                               
                     <td data-label="Expiration"><?php echo date('M d, Y',strtotime($val->post_date)) ?></td>
                    <td data-label="Value"><?php echo  date("M d, Y", strtotime("+1 month",strtotime($val->post_date))) ?></td>
                    <td data-label="Value">Grammer Quiz</td>



                    <td data-column="Last Name"><a class="confirms btn btn__active btn-startquiz" href="#" data-id="<?= $val->ID ?>" data-nonce="<?= $nonce ?>" data-quizstatus=""> Start Quiz</a></td>

                    </tr>
                        <?php
                             }

                            }
                        }
                    else{
                        ?>

                        <tr>
                            <td colspan="5">
                                No data found
                            </td>
                        </tr>

                    <?php } ?>

                    </tbody>
                </table>

            </div>
        </div>

        <div class="student_name_row">
            <div class="student_name">
                <h2>Previous Quiz</h2>
            </div>
            <div class="request_btn">
                <!--                <a href="#">Send Quiz Invite</a>-->
            </div>

        </div>
        <div class="quiz_section">
            <div class="quiz_head">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Quiz Title</th>
                        <th>Status</th>
                        <th>Start Date</th>
                        <th>Due Date</th>
                        <th>Quiz Type</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    if($user_id != 0 && count($posts) > 0){

                        foreach($posts as $val){
                            $quiz_test_id = get_option( 'quiz_test' );
                            $url =  get_permalink( $quiz_test_id );
                            $quiz_id = $val->ID;
                            $user_id = get_current_user_id();
                            $args = array(
                                'numberposts'   => -1,
                                'post_type'     => 'quizresult',
                                'meta_query'    => array(
                                    'relation'      => 'AND',
                                    array(
                                        'key'       => 'quiz_id',
                                        'compare'   => '=',
                                        'value'     => $quiz_id,
                                    ),
                                    array(
                                        'key'       => 'user',
                                        'compare'   => '=',
                                        'value'     => $user_id,
                                    )
                                )
                            );
                            $get_result = get_posts($args);
                            $mcqs_end = get_post_meta($get_result[0]->ID , 'mcqs_end' ,true );
                            $video_end = get_post_meta($get_result[0]->ID , 'video_end' ,true );
                            $percentage = get_post_meta($get_result[0]->ID , 'percentage' ,true );
                            if(empty($get_result) == false && $mcqs_end && $video_end){
                                ?>
                                <tr>
                                    <td data-label="Contact ID"><?= $val->post_title ?></td>

                                        <td data-label="Power">Completed</td>


                                
                                    <td data-label="Expiration"><?php echo date('M d, Y',strtotime($val->post_date)) ?></td>
                                    <td data-label="Value"><?php echo  date("M d, Y", strtotime("+1 month",strtotime($val->post_date))) ?></td>
                                    <td data-label="Value">Grammer Quiz</td>



                                    <td data-column="Last Name"><a class="btn btn__active complete-btn" href="#" data-id="<?= $val->ID ?>" data-nonce="<?= $nonce ?>"> Complete</a></td>

                                </tr>
                                <?php
                            }

                        }
                    }
                    else{
                        ?>

                        <tr>
                            <td colspan="5">
                                No data found
                            </td>
                        </tr>

                    <?php } ?>

                    </tbody>
                </table>

            </div>
        </div>

        <div class="comment_section">
        </div>

    </div>

</div>

<?php
get_footer();
?>
<style>
    .jconfirm-box-container{
        width: 600px;
        margin: 0 auto;
    }
</style>
<script>

    jQuery(".confirms").confirm({
        title:"Do you want to start the Quiz?",
        text:"Please confirm if you want to start",
        buttons: {
            confirm: function (button) {
                let quiz_id = jQuery(this)[0].$target[0].dataset.id;
                let nonce = jQuery(this)[0].$target[0].dataset.nonce;

                var url = "<?= $url . '?quiz='?>"+quiz_id;
                jQuery.ajax({
                    type : "post",
                    url : '<?= admin_url('admin-ajax.php'); ?>',
                    data : {
                        action: "my_quiz_test",
                        quiz_id : quiz_id,
                        nonce: nonce
                    },
                    success: function(response) {
                        console.log(response);
                        var data = JSON.parse(response);
                        if(data.type === "success"){
                            window.location.href= data.url
                        }else{
                            console.log(data.type);
                        }
                    }
                })
            },
            cancel: function () {
                //   jQuery.alert('Canceled!');
            },
        },
    });
</script>