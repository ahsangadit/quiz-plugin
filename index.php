<?php

/**
 * Plugin Name:       vgc
 * Plugin URI:        https://example.com/plugins/the-basics/
 * Description:       Quiz plugin for quiz management
 * Version:           1.10.3
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Talha Ahmed
 * Author URI:        https://author.example.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Update URI:        https://example.com/my-plugin/
 * Text Domain:       vgc
 * Domain Path:       /languages
 *
 * newplugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

 * newplugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
along with {Plugin Name}. If not, see {URI to Plugin License}.

 */
if (!defined('WPINC')) {

    die;

}
if (!defined('VGC_PLUGIN_DIR')) {

    define('VGC_PLUGIN_DIR', plugin_dir_url(__FILE__));

}
if (!function_exists('vgc_plugin_scripts_admin')) {

    function vgc_plugin_scripts_admin()
    {
        wp_enqueue_style('vgc-css', VGC_PLUGIN_DIR . 'assets/css/main.css');
        wp_enqueue_script('vgc-js', VGC_PLUGIN_DIR . 'assets/js/main.js');
    }

    add_action('admin_enqueue_scripts', 'vgc_plugin_scripts_admin');
}
function enqueue_select2_jquery() {
    wp_register_style( 'select2css', '//cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.css', false, '1.0', 'all' );
    wp_register_script( 'select2', '//cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.js', array( 'jquery' ), '1.0', true );
    wp_enqueue_style( 'select2css' );
    wp_enqueue_script( 'select2' );
}
add_action( 'admin_enqueue_scripts', 'enqueue_select2_jquery' );
if (!function_exists('vgc_plugin_scripts')) {

    function vgc_plugin_scripts()
    {
     //   wp_enqueue_script('vgc-jss', VGC_PLUGIN_DIR . 'assets/js/script.js');
        wp_register_style( 'confirdm', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css', false, '1.0', 'all' );
        wp_register_script( 'confirsc', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js', array( 'jquery' ), '1.0', true );
        wp_enqueue_style('vgcfront-css', VGC_PLUGIN_DIR . 'assets/css/frontmain.css');
        wp_enqueue_style('questions-css', VGC_PLUGIN_DIR . 'assets/css/style.css');
        wp_enqueue_style( 'confirdm' );
        wp_enqueue_script( 'confirsc' );
    }

    add_action('wp_enqueue_scripts', 'vgc_plugin_scripts');
}

require __DIR__ . '/inc/admin/functions.php';
// programmatically create some basic pages, and then set Home and Blog
// setup a function to check if these pages exist
function the_slug_exists($post_name) {
    global $wpdb;
    if($wpdb->get_row("SELECT post_name FROM wp_posts WHERE post_name = '" . $post_name . "'", 'ARRAY_A')) {
        return true;
    } else {
        return false;
    }
}

 function dd($print) {
    echo '<pre>';
    var_dump($print);
    echo '<pre>';
    exit();
}

function deactivate_plugin(){
    remove_role( 'agent' );
    remove_role( 'student' );
    $vgc_student_reg_page_id = get_option('vgc_student_reg_page_id',true );
    wp_update_post(array(
        'ID'    =>  $vgc_student_reg_page_id,
        'post_status'   =>  'draft'
    ));
   $user_account_id = get_option( 'user_account' );
   $quiz_test_id    = get_option( 'quiz_test' );
   
   wp_delete_post($user_account_id);
   wp_delete_post($quiz_test_id);

}

function activate_pluginn(){
       $user_account = array('post_title'    => 'User Account',
            'post_status'   => 'publish',
            'post_type'     => 'page'
       );
       $quiz_test = array('post_title'    => 'Quiz Test',
        'post_status'   => 'publish',
        'post_type'     => 'page'
       );

       $user_account_id = wp_insert_post( $user_account );
       $quiz_test_id = wp_insert_post( $quiz_test );

       update_option( 'user_account',   $user_account_id );
       update_option( 'quiz_test',      $quiz_test_id );

}

function strip_param_from_url( $url, $param ) {
    $base_url = strtok($url, '?');              // Get the base url
    $parsed_url = parse_url($url);              // Parse it
    $query = $parsed_url['query'];              // Get the query string
    parse_str( $query, $parameters );           // Convert Parameters into array
    unset( $parameters[$param] );               // Delete the one you want
    $new_query = http_build_query($parameters); // Rebuilt query string
    return $base_url.'?'.$new_query;            // Finally url is ready
}

//register_activation_hook(__FILE__, 'add_student_register_page');
register_deactivation_hook( __FILE__, 'deactivate_plugin' );

register_activation_hook( __FILE__, 'activate_pluginn' );

//add programs
require __DIR__ . '/inc/admin/programs.php';
//add test
require __DIR__ . '/inc/admin/test.php';
//add questions
require __DIR__ . '/inc/admin/questions.php';
//add user custom fields for backend
require __DIR__ . '/inc/admin/users_customfields.php';
//add backend filters
require __DIR__ . '/inc/admin/filters.php';
//add student registration shortcode
require __DIR__ . '/inc/admin/student_register_shortcode.php';
//add agent  registration shortcode
require __DIR__ . '/inc/admin/agent_register_shortcode.php';
//add agent login  shortcode
require __DIR__ . '/inc/admin/agent_login_shortcode.php';
//add agent test create
require __DIR__ . '/inc/agent/functions.php';
//ahsan work
require __DIR__ . '/inc/admin/quiz.php';


